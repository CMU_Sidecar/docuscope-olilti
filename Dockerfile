FROM gradle:jdk11 as builder
ENV GRADLE_ENV=-Dorg.gradle.daemon=false
COPY --chown=gradle:gradle . /home/gradle
WORKDIR /home/gradle
RUN gradle war

FROM jboss/wildfly:24.0.0.Final
ADD standalone.xml /opt/jboss/wildfly/standalone/configuration/standalone.xml
COPY --from=builder /home/gradle/wildfly/build/libs/*.war /opt/jboss/wildfly/standalone/deployments/
