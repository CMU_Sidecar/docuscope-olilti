deploydir="$(pwd)/wildfly/target"
containerid=
# Deploy war file in wildfly/target manually
echo "Deploying war file into docker container ..."
#docker cp wildfly/target/cmlh.war wildfly-app:/opt/jboss/wildfly/standalone/deployments/cmlh.war
#docker run -it -p 8080:8080 -v /home/vvelsen/CMLH/cmlh-pipeline/chaperone/wildfly/target:/opt/jboss/wildfly/standalone/deployments/:rw jboss/wildfly
docker cp $deploydir/cmlh.war $containerid:/opt/jboss/wildfly/standalone/deployments/cmlh.war