package org.imsglobal.lti.launch;

import org.apache.http.client.methods.HttpPost;

import java.io.IOException;
import java.util.*;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpUpgradeHandler;
import javax.servlet.http.Part;

/**
 * @author  Paul Gray
 */
public class MockHttpPost extends BaseMockHttpServletRequest {

    private HttpPost post;

    public MockHttpPost(HttpPost req) throws Exception {
        super(req);
        this.post = req;
    }

    @Override
    public String getMethod() {
        return "POST";
    }

    @Override
    public Map getParameterMap() {
        //todo: merge this with multipart body
        String q = this.getQueryString();
        if(q == null) {
            return new HashMap();
        } else {
            return this.getQueryMap(this.getQueryString());
        }
    }

    @Override
    public boolean authenticate(HttpServletResponse arg0) throws IOException, ServletException {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public String changeSessionId() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public Part getPart(String arg0) throws IOException, ServletException {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public Collection<Part> getParts() throws IOException, ServletException {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public void login(String arg0, String arg1) throws ServletException {
      // TODO Auto-generated method stub
      
    }

    @Override
    public void logout() throws ServletException {
      // TODO Auto-generated method stub
      
    }

    @Override
    public <T extends HttpUpgradeHandler> T upgrade(Class<T> arg0) throws IOException, ServletException {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public AsyncContext getAsyncContext() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public long getContentLengthLong() {
      // TODO Auto-generated method stub
      return 0;
    }

    @Override
    public DispatcherType getDispatcherType() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public ServletContext getServletContext() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public boolean isAsyncStarted() {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public boolean isAsyncSupported() {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public AsyncContext startAsync() throws IllegalStateException {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public AsyncContext startAsync(ServletRequest arg0, ServletResponse arg1) throws IllegalStateException {
      // TODO Auto-generated method stub
      return null;
    }
}
