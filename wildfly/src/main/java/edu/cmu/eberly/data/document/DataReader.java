package edu.cmu.eberly.data.document;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.servlet.http.Part;

import edu.cmu.eberly.drivers.activity.OLIActivityDocuscope;

/**
 * @author vvelsen
 */
public class DataReader {

	private static Logger M_log = Logger.getLogger(OLIActivityDocuscope.class.getName());

	/**
	 * @param file
	 * @return
	 */
	public static byte[] readFromPart(Part file) {

		byte[] byteBuffer = null;
		InputStream filecontent = null;

		try {
			filecontent = file.getInputStream();
			M_log.info("Getting file data ..");
		} catch (IOException e) {
			M_log.info("Error uploading file IOException: " + e.getMessage());
			return (byteBuffer);
		}

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[16384];

		try {
			while ((nRead = filecontent.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
		} catch (IOException e) {
			M_log.info("Error uploading file IOException: " + e.getMessage());
			return (byteBuffer);
		}

		try {
			buffer.flush();
		} catch (IOException e) {
			M_log.info("Error uploading file IOException: " + e.getMessage());
			return (byteBuffer);
		}

		byteBuffer = buffer.toByteArray();

		return (byteBuffer);
	}
}
