package edu.cmu.eberly.data;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import edu.cmu.eberly.tools.ServletTools;
//import edu.cmu.eberly.tools.StringTools;

/**
 * @author vvelsen
 */
public class MySQLDriverFilemanager extends MySQLDriver {

  private static Logger M_log = Logger.getLogger(MySQLDriverFilemanager.class.getName());

  /**
   *
   */
  protected void configureEnvironment() {
    super.configureEnvironment();
    dbTable = "filesystem";
  }

  /**
   * We're mapping instances from a Db representation to JSON representing a
   * OLILTIFileObject object
   *
   * @throws Exception
   */
  public void prepTables() throws Exception {
    M_log.info("prepTables ()");
  }

  /**
   * DELETE FROM `table_name` [WHERE condition];
   */
  public int removeFile(String targetFile) {
    M_log.info("removeFile ()");

    int result = -1;

    Statement statement = createStatement();

    if (statement == null) {
      return (result);
    }

    String statementString = "DELETE FROM " + dbTable + " WHERE id = UUID_TO_BIN(\"" + targetFile + "\");";

    result = executeUpdate(statement, statementString);

    return (result);
  }

  /**
   * We're mapping instances of OLILTIFileObject to a Db representation
   *
   * @param newFile
   */
  public int storeFileDocX(OLILTIFileObject newFile) {
    M_log.info("storeFileDocX ()");

    int result = -1;
    
    M_log.info("Creation time: " + newFile.fCreated);
    
    String statementString = "INSERT INTO " + dbTable
        + "(id, name, assignment, owner, fullname, ownedby, content)" 
        + " SELECT"
        + " UUID_TO_BIN(?)," // template index: 1, column: id
        + " ?," // template index: 2, column: filename
        + " (SELECT id FROM assignments WHERE"
        + " oli_id=UNHEX(?) LIMIT 1)," // template index: 3, column: assignment
        + " ?," // template index: 4, column: owner
        //+ " ?," // created. time from client is unreliable and should not be used
        + " ?," // template index: 5, column: fullname
        + " ?," // template index: 6, column: ownedby
        + " ?" // template index: 7, column: content
        + " WHERE EXISTS (SELECT * FROM assignments"
        + " WHERE oli_id=UNHEX(?))"; // template index: 8, column: assignment
    
    M_log.info("Intermediate: " + statementString);
    M_log.info(newFile.fCreated);

    if ((conn == null) || (active == false)) {
      M_log.info("Internal error: no DB connection available. Attempting to connect...");
      if (connect() == false) {
        return (-1);
      }
      M_log.info("Reestablished connection to the database.");
    }
    
    try (PreparedStatement statement = conn.prepareStatement(statementString)) {
      statement.setString(1, newFile.fId);
      statement.setString(2, newFile.fName);
      statement.setString(3, newFile.fAssignment);
      statement.setString(4, newFile.fOwner);
      statement.setString(5, newFile.fFullname);
      statement.setString(6, newFile.fOwnedBy);
      statement.setBytes(7, newFile.raw);
      statement.setString(8, newFile.fAssignment);
      result = statement.executeUpdate();
    } catch (SQLException e) {
      M_log.info("DB Error: " + e.getMessage());
      e.printStackTrace();
    }
    
    M_log.info("File store result: " + result);
    
    return (result);
  }

  /**
   * We're mapping instances of OLILTIFileObject to a Db representation
   *
   * @param newFile
   */
  public int storeFilePDF(OLILTIFileObject newFile) {
    M_log.info("storeFilePDF ()");

    int result = -1;

    String statementString = "INSERT INTO " + dbTable
        + "(id, name, assignment, owner, fullname, ownedby, content)" 
        + " SELECT" + " UUID_TO_BIN(?)," // 1, id
        + " ?," // 2, filename
        + " (SELECT id FROM assignments WHERE oli_id=UNHEX(?) LIMIT 1)," // 3, assignment
        + " ?," // 4, owner
        + " ?," // 5, fullname
        + " ?," // 6, ownedby
        + " ?" // 7, content
        + " WHERE EXISTS (SELECT * FROM assignments WHERE oli_id=UNHEX(?))"; // 8, assignment

    if ((conn == null) || (active == false)) {
      M_log.info("Internal error: no DB connection available. Attempting to connect...");
      if (connect() == false) {
        return (-1);
      }
      M_log.info("Reestablished connection to the database.");
    }

    try (PreparedStatement statement = conn.prepareStatement(statementString)) {
      statement.setString(1, newFile.fId);
      statement.setString(2, newFile.fName);
      statement.setString(3, newFile.fAssignment);
      statement.setString(4, newFile.fOwner);
      statement.setString(5, newFile.fFullname);
      statement.setString(6, newFile.fOwnedBy);
      statement.setBytes(7, newFile.raw);
      statement.setString(8, newFile.fAssignment);
      result = statement.executeUpdate();
    } catch (SQLException e) {
      M_log.info("DB Error: " + e.getMessage());
      e.printStackTrace();
    }
    return (result);
  }

  /**
   * We're mapping instances of OLILTIFileObject to a Db representation
   *
   * @param newFile
   */
  public OLILTIFileObject getFileBinary(String anId) {
    M_log.info("getFileBinary ()");

    ResultSet resultSet = null;
    Statement statement = createStatement();

    if (statement == null) {
      M_log.info("Error: Unable to create statement");
      return (null);
    }

    String statementString = selectFile(anId);

    M_log.info("Executing: " + statementString);

    OLILTIFileObject retrievedFile = null;

    resultSet = executeQuery(statement, statementString);

    if (resultSet == null) {
      return (null);
    }
    retrievedFile = new OLILTIFileObject();
    try {
      resultSet.next();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    try {
      retrievedFile.fId = resultSet.getString("id");
      retrievedFile.fName = resultSet.getString("name");
      retrievedFile.fType = resultSet.getString("type");
      retrievedFile.fCreated = DateFormat.getDateInstance().format(resultSet.getDate("created"));
      retrievedFile.fCreatedRaw = Integer.toString(resultSet.getInt("createdraw"));
      retrievedFile.fSize = Integer.toString(resultSet.getInt("size"));
      retrievedFile.fCreatedRaw = resultSet.getString("createdraw");
      retrievedFile.fSize = resultSet.getString("size");
      retrievedFile.fCourse = resultSet.getString("course");
      retrievedFile.fOwner = resultSet.getString("owner");
      retrievedFile.fState = resultSet.getString("state");
      retrievedFile.fOwnedBy = resultSet.getString("ownedby");
      retrievedFile.fFullname = resultSet.getString("fullname");
      retrievedFile.fAssignment = resultSet.getString("assignment");
      retrievedFile.raw = resultSet.getBytes("content");
    } catch (SQLException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    closeStatement(statement, resultSet);

    return (retrievedFile);
  }

  /**
   * We're mapping instances of OLILTIFileObject to a Db representation
   *
   * @param newFile
   */
  public OLILTIFileObject getFile(String anId) {
    OLILTIFileObject retrievedFile = getFileBinary(anId);
    retrievedFile.fData = Base64.getEncoder().encodeToString(retrievedFile.raw);

    return (retrievedFile);
  }

  /**
   * @return
   */
  public String selectAll() {
    return "SELECT BIN_TO_UUID(f.id) AS id, f.name, 'file' AS type, f.created, "
        + "UNIX_TIMESTAMP(f.created) AS createdraw, LENGTH(f.content) AS size, "
        + "a.course, LOWER(HEX(a.oli_id)) as assignment, d.name AS dictionary, "
        + "f.owner, f.state, f.fullname, f.content FROM "
        + dbName + "." + dbTable + " AS f, " + dbName + ".assignments AS a, "
        + dbName
        + ".dictionaries AS d WHERE f.assignment=a.id AND a.dictionary=d.id";
  }

  /**
   * SQL statement for retrieving a file.
   * 
   * @return
   */
  public String selectFile(String anId) {
    return "select BIN_TO_UUID(f.id) AS id, f.name, 'file' AS type, f.created, "
        + "UNIX_TIMESTAMP(f.created) AS createdraw, LENGTH(f.content) AS size, "
        + "a.course, f.owner, f.state, f.ownedby, "
        + "LOWER(HEX(a.oli_id)) as assignment, f.fullname, content FROM "
        + dbName + "." + dbTable + " AS f, " + dbName
        + ".assignments AS a WHERE f.assignment=a.id AND f.id=UUID_TO_BIN('"
        + anId + "')";
  }

  /**
   * Not ideal, we'll implement a better query in the future.
   * https://dev.mysql.com/doc/refman/8.0/en/example-maximum-column-group-row.html
   * 
   * https://stackoverflow.com/questions/1313120/retrieving-the-last-record-in-each-group-mysql
   * https://www.geeksforgeeks.org/mysql-partition-by-clause/
   *
   * @return
   */
  public String selectAllStudent(String anId, String anAssignmentId) {
    M_log.info("selectAllStudent (" + anId + "," + anAssignmentId + ")");

    /* 
    if (anId.equalsIgnoreCase("-1") == true) {
      return ("SELECT s1.name, BIN_TO_UUID(s1.id) AS id, 'file' AS type, s1.created, UNIX_TIMESTAMP(s1.created) AS createdraw, a.course, s1.owner, s1.state, s1.ownedby, s1.fullname, LENGTH(s1.content) AS size FROM "
          + dbTable
          + " AS s1, assignments AS a WHERE UNIX_TIMESTAMP(s1.created) = (SELECT MAX(UNIX_TIMESTAMP(s2.created)) FROM "
          + dbTable
          + " AS s2 WHERE s1.owner = s2.owner AND s1.ownedby='student' AND s1.assignment=(SELECT id FROM assignments WHERE oli_id=UNHEX('"
          + anAssignmentId + "'))) AND s1.assignment=a.id");
    }
    */
    
    if (anId.equalsIgnoreCase("-1") == true) {
      return ("SELECT s1.name, BIN_TO_UUID(s1.id) AS id, 'file' AS type, "
              + "s1.created, UNIX_TIMESTAMP(s1.created) AS createdraw, "
              + "a.course, s1.owner, s1.state, s1.ownedby, s1.fullname, "
              + "LENGTH(s1.content) AS size FROM filesystem AS s1, "
              + "assignments AS a WHERE (s1.ownedby = 'student') "
              + "AND s1.assignment = (SELECT id FROM assignments "
              + "WHERE oli_id = UNHEX('" + anAssignmentId + "')) "
              + "AND s1.assignment = a.id "
              + "ORDER BY s1.created DESC");
    }
    
    /*
    if (anId.equalsIgnoreCase("-1") == true) {
      return "select name, BIN_TO_UUID(id) AS id, created, UNIX_TIMESTAMP(created) AS createdraw, owner, state, ownedby, fullname, LENGTH(content) AS size, 'file' AS type from " + dbName + "." + dbTable + " WHERE owner='" + anId + "' AND ownedby='student' AND assignment=(SELECT id FROM assignments WHERE oli_id=UNHEX('" + anAssignmentId + "'))";
    }
    */

    // return "SELECT f.name, BIN_TO_UUID(f.id) AS id, 'file' AS type, f.created,
    // UNIX_TIMESTAMP(f.created) AS createdraw, LENGTH(f.content) AS size, a.course,
    // f.owner, f.state, f.ownedby, f.fullname FROM " + dbName + "." + dbTable + "
    // AS f , "+dbName+".assignments AS a WHERE f.owner='" + anId + "' AND
    // f.assignment=a.id AND f.ownedby='student' AND a.oli_id=UNHEX('" +
    // anAssignmentId + "') LIMIT 1";
    return "SELECT f.name, BIN_TO_UUID(f.id) AS id, 'file' AS type, f.created, "
        + "UNIX_TIMESTAMP(f.created) AS createdraw, LENGTH(f.content) AS size, "
        + "a.course, f.owner, f.state, f.ownedby, f.fullname FROM "
        + dbName + "." + dbTable + " AS f , " + dbName
        + ".assignments AS a WHERE f.owner='" + anId
        + "' AND f.assignment=a.id AND f.ownedby='student' "
        + "AND a.oli_id=UNHEX('" + anAssignmentId + "') "
        + "ORDER BY f.created DESC";
  }

  /**
   * Not ideal, we'll implement a better query in the future.
   * https://dev.mysql.com/doc/refman/8.0/en/example-maximum-column-group-row.html
   * 
   * https://stackoverflow.com/questions/1313120/retrieving-the-last-record-in-each-group-mysql
   * https://www.geeksforgeeks.org/mysql-partition-by-clause/
   *
   * @return
   */
  public String selectAllStudentFilesInAssignment(String anAssignmentId) {
    M_log.info("selectAllStudent (" + anAssignmentId + ")");

    return "SELECT * FROM filesystem "
        + "WHERE assignment = (SELECT id from assignments WHERE oli_id=UNHEX('"
        + anAssignmentId + "')) AND ownedby='student' ";
  }

  /**
   * Not ideal, we'll implement a better query in the future.
   * https://dev.mysql.com/doc/refman/8.0/en/example-maximum-column-group-row.html
   *
   * @return
   */
  public String selectAllStudentPDF(String anId, String anAssignmentId) {
    if (anId.equalsIgnoreCase("-1") == true) {
      return ("SELECT pdf,name FROM " + dbTable
              + " s1 WHERE s1.id = (SELECT s2.id FROM " + dbTable
              + " s2 WHERE s1.owner = s2.owner AND assignment=(SELECT id FROM assignments WHERE oli_id=UNHEX('"
              + anAssignmentId + "')) ORDER BY s2.created DESC LIMIT 1)");
    }
    return "SELECT pdf,name FROM " + dbName + "." + dbTable
        + " WHERE owner='" + anId
        + "' AND assignment=(SELECT id FROM assignments WHERE oli_id=UNHEX('"
        + anAssignmentId + "')) "
        + " ORDER BY created DESC LIMIT 1";
  }

  /**
   * @return
   */
  public String selectAllInstructor(String anId, String anAssignmentId) {
    if (anId.equalsIgnoreCase("-1") == true) {
      return "SELECT f.name, BIN_TO_UUID(f.id) AS id, 'file' AS type, "
          + "f.created, UNIX_TIMESTAMP(f.created) AS createdraw, "
          + "LENGTH(f.content) AS size, a.course, f.owner, f.state, f.ownedby, "
          + "f.fullname FROM "
          + dbName + "." + dbTable + " AS f, " + dbName
          + ".assignments AS a WHERE f.assignment=a.id AND "
          + "f.ownedby='instructor' AND a.oli_id=UNHEX('" + anAssignmentId
          + "')";
    }
    return "SELECT f.name, BIN_TO_UUID(f.id) AS id, 'file' AS type, f.created, "
        + "UNIX_TIMESTAMP(f.created) AS createdraw, LENGTH(f.content) AS size, "
        + "a.course, f.owner, f.state, f.ownedby, f.fullname FROM "
        + dbName + "." + dbTable + " AS f , " + dbName
        + ".assignments AS a WHERE f.owner='" + anId
        + "' AND f.assignment=a.id AND f.ownedby='instructor' "
        + "AND a.oli_id=UNHEX('" + anAssignmentId + "')";
  }

  /**
   * @return
   */
  public String selectAllPending() {
    return "SELECT BIN_TO_UUID(id) AS id FROM " + dbName + "." + dbTable
        + " WHERE state='pending'";
  }

  /**
   * @return
   */
  public String selectAllSubmitted() {
    return "SELECT BIN_TO_UUID(f.id), LOWER(HEX(a.oli_id)) AS assignment, "
        + "d.name AS dictionary FROM " + dbName + "."
        + dbTable + "AS f, " + dbName + ".assignments AS a, " + dbName
        + ".dictionaries AS d WHERE f.state='submitted' AND "
        + "a.id = f.assignment AND a.dictionary = d.id";
  }

  /**
   * @return
   */
  public String selectAllProcessed() {
    return "SELECT BIN_TO_UUID(f.id), a.course, LOWER(HEX(a.oli_id)), f.owner, "
        + "d.name AS dictionary, f.content FROM "
        + dbName + "." + dbTable + " AS f, " + dbName + ".assignments AS a, "
        + dbName
        + ".dictionaries AS d WHERE f.state='processed' AND "
        + "f.assignment = a.id AND a.dictionary = d.id";
  }

  /**
   * This will change the state of an existing file from 0 to 1, meaning it goes
   * from pending to submitted.
   *
   * @param anId
   */
  public int submitFile(String anId) {
    M_log.info("storeFile ()");

    int result = -1;

    Statement statement = createStatement();

    if (statement == null) {
      return (result);
    }

    String statementString = "UPDATE " + dbTable
        + " SET state = 'submitted' WHERE id = UUID_TO_BIN('" + anId + "');";

    result = executeUpdate(statement, statementString);

    return (result);
  }

  /**
   *
   * @param fileID
   * @param fileJSON
   */
  public int updateEntry(String fileID, String fileJSON) {
    M_log.info("updateEntry ()");

    int result = -1;

    Statement statement = createStatement();

    if (statement == null) {
      return (result);
    }

    String statementString = "UPDATE " + dbTable
        + " SET state = 'processed', processed='" + fileJSON
        + "' WHERE id = UUID_TO_BIN('" + fileID + "');";

    result = executeUpdate(statement, statementString);

    return (result);
  }

  /**
   * We're mapping instances of OLILTIFileObject to a Db representation
   *
   * @param newFile
   */
  public ArrayList<String> getDictionaries() {
    M_log.info("getDictionaries ()");

    ArrayList<String> dictionaries = new ArrayList<String>();

    ResultSet resultSet = null;
    Statement statement = createStatement();

    if (statement == null) {
      M_log.info("Can't create statement, bump");
      return (null);
    }

    String statementString = "select name from " + dbName + ".dictionaries";

    resultSet = executeQuery(statement, statementString);

    if (resultSet != null) {
      try {
        while (resultSet.next()) {
          String dictionaryEntry = resultSet.getString("name");

          dictionaries.add(dictionaryEntry);
        }
      } catch (Exception e) {
        M_log.info("result: " + e.getMessage());
      } finally {
        closeStatement(statement, resultSet);
      }
    }

    return (dictionaries);
  }

  /**
   * @param assignmentID
   * @return
   */
  public String getDictionary(String assignmentID) {
    M_log.info("getDictionary (" + assignmentID + ")");

    ResultSet resultSet = null;
    String testName = "";

    Statement statement = createStatement();

    if (statement == null) {
      M_log.info("Statement is null, can't get dictionary from db");
      return (null);
    }

    String statementString = "SELECT d.name as dictionary FROM "
        + "dictionaries AS d, assignments AS a WHERE a.oli_id=UNHEX(\""
        + assignmentID + "\") AND a.dictionary=d.id LIMIT 1";

    try {
      resultSet = executeQuery(statement, statementString);
      while (resultSet.next()) {
        testName = resultSet.getString("dictionary");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      closeStatement(statement, resultSet);
    }

    return (testName);
  }

  /**
   * DEPRECATED
   * 
   * @param anId
   */
  /*
   * public int addDictionary(String anId) { M_log.info("addDictionary (" + anId +
   * ")");
   * 
   * //ResultSet resultSet = null; int result = -1; Statement statement =
   * createStatement();
   * 
   * if (statement == null) { return (result); }
   * 
   * String statementString = "INSERT INTO dictionaries (name) VALUES ('" + anId +
   * "')";
   * 
   * result = executeUpdate(statement,statementString);
   * 
   * return (result); }
   */

  /**
   * @param parameter
   * @param parameter2
   * @param parameter3
   */
  public void loadPreview(String aCourse, String aUser, String anAssignmentId, HttpServletResponse response) {
    M_log.info("loadPreview ()");

    ResultSet resultSet = null;
    Statement statement = createStatement();

    if (statement == null) {
      return;
    }

    String statementString = selectAllStudentPDF(aUser, anAssignmentId);

    resultSet = executeQuery(statement, statementString);

    // String pdf="";
    byte[] buffer = null;
    String filename = "";

    try {
      if (resultSet.next()) {
        buffer = resultSet.getBytes("pdf");
        filename = resultSet.getString("name");
      }
    } catch (SQLException e) {
      M_log.info("Internal error, unable to retrieve file: " + e.getMessage());
      return;
    }

    if (buffer == null || buffer.length == 0) {
      M_log.info("Internal error, unable to retrieve file!");
      return;
    }

    M_log.info("Decoding " + buffer.length + " bytes ...");

    // byte[] buffer = Base64.getDecoder().decode(raw);

    String mimeType = ServletTools.getFileMimetype("preview.pdf"); // It's always the same, so we should probably just
                                                                   // hardcode it

    M_log.info("Writing file to browser with mime type: " + mimeType + " for " + buffer.length + " bytes");

    // response.setHeader("Content-disposition", "attachment;filename=\"" +
    // "preview.pdf" + "\"");
    // response.setContentType(mimeType);

    OutputStream out = null;

    try {
      out = response.getOutputStream();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    try {
      out.write(buffer, 0, buffer.length);
    } catch (IOException e) {
      M_log.info("Error writing to output stream: " + e.getMessage());
    }

    M_log.info("Data written, flusing buffer ...");

    try {
      out.flush();
    } catch (IOException e) {
      M_log.info("Error flushing output stream: " + e.getMessage());
    }

    M_log.info("Closing output stream ...");

    try {
      out.close();
    } catch (IOException e) {
      M_log.info("Error closing output stream: " + e.getMessage());
    }

    closeStatement(statement, resultSet);
  }

  /**
   * @param pdfData
   */
  public int storePDF(byte[] pdfData, String fileID) {
    int result = -1;
    String statementString = "UPDATE " + dbTable + " SET pdf=? WHERE id=UUID_TO_BIN(?);";
    try (PreparedStatement statement = conn.prepareStatement(statementString)) {
      statement.setBytes(1, pdfData);
      statement.setString(2, fileID);
      result = statement.executeUpdate();
    } catch (SQLException e) {
      M_log.info("DB Error: " + e.getMessage());
      e.printStackTrace();
      result = -1;
    }
    return result;
  }

  /**
   * @param pdfEncoded
   */
  public int storeDocX(byte[] docx, String fileID) {
    M_log.info("storeDocX ()");

    int result = -1;

    String statementString = "UPDATE " + dbTable + " SET content=? WHERE id = UUID_TO_BIN(?);";
    try (PreparedStatement statement = conn.prepareStatement(statementString)) {
      statement.setBytes(1, docx);
      statement.setString(2, fileID);
      result = statement.executeUpdate();
    } catch (SQLException e) {
      M_log.info("DB Error: " + e.getMessage());
      e.printStackTrace();
      result = -1;
    }
    return (result);
  }

  /**
   * @param aCompany
   * @return
   */
  public int assignmentExists(String anAssignment) {
    M_log.info("assignmentExists (" + anAssignment + ")");

    int result = -1;

    Statement statement = createStatement();

    if (statement == null) {
      M_log.info("Error creating statement!");
      return (result);
    }

    String statementString = "SELECT * FROM assignments WHERE oli_id=UNHEX('"
        + anAssignment + "')";

    ResultSet rSet = executeQuery(statement, statementString);
    
    int total=0;
    
    try {
      while (rSet.next()==true) {
        total++;
      }
    } catch (SQLException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    
    M_log.info("Total number of rows retrieved: " + total);
    
    /*
    try {
      if (rSet.next()) {
        M_log.info("Assignment exists");
        return (1);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    
    M_log.info("Assignment does NOT exist");
    */

    return (total);
  }

  /**
   *
   * @param fileID
   * @param fileJSON
   */
  public int createAssignment(String anAssignment, String aDictionary, String anAssignmentName, String aCourse,
      String anInstructor) {
    M_log.info("createAssignment ()");

    if (assignmentExists(anAssignment) > 0) {
      M_log.info("Assignment already exists, bump");
      return (1);
    }

    int result = -1;

    Statement statement = createStatement();

    if (statement == null) {
      return (result);
    }

    String statementString = "INSERT INTO assignments "
        + "(oli_id, dictionary, name, course, instructor, showstudent, showmodel)"
        + " VALUES ("
        + "UNHEX(\"" + anAssignment + "\"), "
        + "(SELECT id FROM dictionaries WHERE name=\"" + aDictionary + "\" LIMIT 1), "
        + "\"" + anAssignmentName + "\", "
        + "\"" + aCourse + "\","
        + "\"" + anInstructor + "\", TRUE, FALSE) "
        + "ON DUPLICATE KEY UPDATE dictionary=(SELECT id FROM dictionaries "
        + "WHERE name=\"" + aDictionary + "\" LIMIT 1)";

    M_log.info(statementString);

    result = executeUpdate(statement, statementString);
    this.resetAssignmentTags(anAssignment);

    return (result);
  }

  /**
   *
   * @param fileID
   * @param fileJSON
   */
  public int updateAssignmentDictionary(String anAssignment, String aDictionary, String anAssignmentName, String aCourse, String anInstructor) {
    M_log.info("updateAssignmentDictionary ("+aDictionary+")");

    int result = -1;

    Statement statement = createStatement();

    if (statement == null) {
      return (result);
    }

    String statementString = "INSERT INTO assignments "
        + "(oli_id,dictionary,name,course,instructor) VALUES (UNHEX(\""
        + anAssignment + "\")," + "(SELECT id FROM dictionaries WHERE name=\""
        + aDictionary + "\" LIMIT 1), " + "\""
        + anAssignmentName + "\"," + "\"" + aCourse + "\", "
        + "\"" + anInstructor + "\") "
        + "ON DUPLICATE KEY UPDATE dictionary="
        + "(SELECT id FROM dictionaries WHERE name=\"" + aDictionary
        + "\" LIMIT 1)";

    M_log.info(statementString);

    result = executeUpdate(statement, statementString);
    this.resetAssignmentTags(anAssignment);

    return (result);
  }

  /**
   *
   * @param assignmentId
   */
  public int resetAssignmentTags(String assignmentId) {
    M_log.info("resetAssignmentTags (" + assignmentId + ")");

    int result = -1;

    Statement statement = createStatement();

    if (statement == null) {
      M_log.info("Internal error: unable to create statement");
      return (result);
    }

    String statementString = "UPDATE " + dbTable
        + " as f, (SELECT id FROM assignments WHERE oli_id=UNHEX('"
        + assignmentId
        + "')) as a SET f.state='pending' WHERE f.assignment = a.id;";

    M_log.info(statementString);

    M_log.info("Resetting assignment state ...");

    result = executeUpdate(statement, statementString);

    return (result);
  }

  /**
   *
   * @param assignmentId
   */
  public int toggleAssignmentShowModel(String assignmentId, String aValue) {
    M_log.info("toggleAssignmentShowModel (" + assignmentId + ")");

    int result = -1;
    int intBoolean = 1;
    
    if (aValue.toLowerCase()=="true") {
      intBoolean=1;
    } else {
      intBoolean=0;
    }

    Statement statement = createStatement();

    if (statement == null) {
      M_log.info("Internal error: unable to create statement");
      return (result);
    }

    String statementString = "UPDATE assignments SET showmodel=" + intBoolean
        + " WHERE oli_id = UNHEX('" + assignmentId
        + "');";

    M_log.info(statementString);

    result = executeUpdate(statement, statementString);

    return (result);
  }

  /**
   * @return
   */
  public Boolean getToggleShowModels(String assignmentId) {
    Statement statement = createStatement();

    if (statement == null) {
      M_log.info("Internal error: unable to create statement");
      return (false);
    }

    String statementString = "SELECT showmodel FROM assignments "
        + "WHERE oli_id=UNHEX('" + assignmentId + "');";

    ResultSet rSet = executeQuery(statement, statementString);

    try {
      if (rSet.next()) {
        M_log.info("Assignment exists");
        /*
        String aValue = rSet.getString("showmodel");

        if (aValue != null) {
          return StringTools.string2Boolean(aValue);
        }
        */
        
        int intBoolean=rSet.getInt("showmodel");
        if (intBoolean==1) {
          return (true);
        } else {
          return (false);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return (false);
  }

  /**
   *
   * @param assignmentId
   */
  public int toggleAssignmentShowStudent(String assignmentId, String aValue) {
    M_log.info("toggleAssignmentShowStudent (" + assignmentId + ")");

    int result = -1;
    int intBoolean = 1;
    
    if (aValue.toLowerCase()=="true") {
      intBoolean=1;
    } else {
      intBoolean=0;
    }

    Statement statement = createStatement();

    if (statement == null) {
      M_log.info("Internal error: unable to create statement");
      return (result);
    }

    String statementString = "UPDATE assignments SET showstudent=" + intBoolean
        + " WHERE oli_id = UNHEX('" + assignmentId
        + "');";

    M_log.info(statementString);

    result = executeUpdate(statement, statementString);

    return (result);
  }

  /**
   * @return
   */
  public Boolean getToggleShowStudent(String assignmentId) {
    M_log.info("getToggleShowStudent (" + assignmentId + ")");

    Statement statement = createStatement();

    if (statement == null) {
      M_log.info("Internal error: unable to create statement");
      return (false);
    }

    String statementString = "SELECT showstudent FROM assignments "
        + "WHERE oli_id=UNHEX('" + assignmentId + "');";

    ResultSet rSet = executeQuery(statement, statementString);

    try {
      if (rSet.next()) {
        M_log.info("Assignment exists");
        
        int intBoolean=rSet.getInt("showstudent");
        M_log.info("showstudent raw: "+ intBoolean);
        if (intBoolean==1) {
          return (true);
        } else {
          return (false);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return (false);
  }
}
