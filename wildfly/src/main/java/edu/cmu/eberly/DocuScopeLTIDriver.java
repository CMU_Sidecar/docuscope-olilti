/**
 *
 */

package edu.cmu.eberly;

import java.util.logging.Logger;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import edu.cmu.eberly.drivers.activity.OLIAPIDocuscope;
import edu.cmu.eberly.drivers.activity.OLIActivityDocuscope;

/**
 * Test implementation of an LTI driver as end-user developers would build them.
 */
@MultipartConfig
@WebServlet(urlPatterns = { "/lti/*", "/api/*" }, loadOnStartup = 1, asyncSupported = true)
public class DocuScopeLTIDriver extends OLILTIMicroservice {

  private static final long serialVersionUID = 1L;

  private static Logger M_log = Logger.getLogger(DocuScopeLTIDriver.class.getName());

  private OLIActivityDocuscope docuscopeActivity = null;
  private OLIAPIDocuscope docuscopeAPI = null;

  /**
   *
   */
  public DocuScopeLTIDriver() {
    M_log.info("DocuScopeLTIDriver ()");

    setLTIBase("lti"); // If you decide to set this, then it has to match the urlPattern configured
                       // above
    setAPIBase("api"); // If you decide to set this, then it has to match the urlPattern configured
                       // above

    docuscopeActivity = new OLIActivityDocuscope();
    registerActivity(docuscopeActivity);

    docuscopeAPI = new OLIAPIDocuscope();
    registerActivity(docuscopeAPI);
  }
}
