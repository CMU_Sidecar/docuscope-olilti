package edu.cmu.eberly.drivers.activity;

import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.cmu.eberly.data.MySQLDriverFilemanager;
import edu.cmu.eberly.data.OLILTIFileObject;
import edu.cmu.eberly.drivers.OLILTIDriverInterface;
import edu.cmu.eberly.tools.LTILMSData;

/**
 * @author vvelsen
 */
public class OLIAPIDocuscope extends OLIActivityBase implements OLILTIDriverInterface {

	private static Logger M_log = Logger.getLogger(OLIAPIDocuscope.class.getName());

	private MySQLDriverFilemanager dbDriver = null;

	/**
	 * 
	 */
	public OLIAPIDocuscope() {
		setAPIControllerCategory("db");
		addAPIControllerName("list");
		addAPIControllerName("update");
		addAPIControllerName("dictionaries");
		addAPIControllerName("document");
		addAPIControllerName("state");
		addAPIControllerName("tags");
	}
	
	/**
	 * 
	 */
	@Override
	public Boolean init() {
		dbDriver = new MySQLDriverFilemanager();
		dbDriver.init();
		return true;
	}	

	/**
	 * 
	 */
	@Override
	public void destroy() {
		M_log.info ("destroy ()");
		dbDriver.close();
	}		

	/**
	 * @return
	 */
	public String processAPICall(LTILMSData lmsData, HttpServletRequest request, HttpServletResponse response) {
		M_log.info("processAPICall (");

		String category = LTILMSData.getControllerCategory(request);
		String controller = LTILMSData.getControllerName(request);
		String action = LTILMSData.getAction(request);

		M_log.info("Processing category: " + category + ", controller: " + controller + ", action: " + action);

		if (dbDriver == null) {
			return ("[]");
		}

		int counter = 0;

		if (category.equalsIgnoreCase("db") == true) {

			// >-------------------------------------------------------------------------

			if ((controller.equalsIgnoreCase("list") == true) && (action.equalsIgnoreCase("all") == true)) {
				String result = "[]";
				ResultSet resultSet = null;
				Statement statement = null;

				try {
					statement = dbDriver.createStatement();

					String statementString = dbDriver.selectAll();

					M_log.info("Executing: " + statementString);

					resultSet = statement.executeQuery(statementString);
				} catch (Exception e) {
					M_log.info("Internal error: " + e.getMessage());
					return ("[]");
				}

				try {
					JsonArrayBuilder json = Json.createArrayBuilder();

					counter = 0;

					while (resultSet.next()) {
						JsonObjectBuilder aFile = Json.createObjectBuilder();
						aFile.add("id", resultSet.getString("id"));
						aFile.add("course", resultSet.getString("course"));
						aFile.add("assignment", resultSet.getString("assignment"));
						aFile.add("owner", resultSet.getString("owner"));
						aFile.add("status", resultSet.getString("state"));
						aFile.add("dictionary", resultSet.getString("dictionary"));
						aFile.add("data", Base64.getEncoder().encodeToString(resultSet.getBytes("content")));

						json.add(aFile);

						counter++;
					}

					result = json.build().toString();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				dbDriver.closeStatement(statement, resultSet);

				M_log.info("Processed " + counter + " entries");

				return (result);
			}

			// >-------------------------------------------------------------------------

			if ((controller.equalsIgnoreCase("list") == true) && (action.equalsIgnoreCase("processed") == true)) {
				String result = "[]";
				ResultSet resultSet = null;
				Statement statement = null;

				try {
					statement = dbDriver.createStatement();

					String statementString = dbDriver.selectAllProcessed();

					M_log.info("Executing: " + statementString);

					resultSet = statement.executeQuery(statementString);
				} catch (Exception e) {
					M_log.info("Internal error: " + e.getMessage());
					return ("[]");
				}

				try {
					JsonArrayBuilder json = Json.createArrayBuilder();

					counter = 0;

					while (resultSet.next()) {
						JsonObjectBuilder aFile = Json.createObjectBuilder();
						aFile.add("id", resultSet.getString("id"));
						aFile.add("course", resultSet.getString("course"));
						aFile.add("assignment", resultSet.getString("assignment"));
						aFile.add("owner", resultSet.getString("owner"));
						aFile.add("dictionary", resultSet.getString("dictionary"));
						aFile.add("data", Base64.getEncoder().encodeToString(resultSet.getBytes("content")));

						json.add(aFile);

						counter++;
					}

					result = json.build().toString();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				dbDriver.closeStatement(statement, resultSet);

				M_log.info("Processed " + counter + " entries");

				return (result);
			}

			// >-------------------------------------------------------------------------

			if ((controller.equalsIgnoreCase("list") == true) && (action.equalsIgnoreCase("submitted") == true)) {
				String result = "[]";
				ResultSet resultSet = null;
				Statement statement = null;

				try {
					statement = dbDriver.createStatement();

					String statementString = dbDriver.selectAllSubmitted();

					M_log.info("Executing: " + statementString);

					resultSet = statement.executeQuery(statementString);
				} catch (Exception e) {
					M_log.info("Internal error: " + e.getMessage());
					return ("[]");
				}

				try {
					JsonArrayBuilder json = Json.createArrayBuilder();

					counter = 0;

					while (resultSet.next()) {
						JsonObjectBuilder aFile = Json.createObjectBuilder();
						aFile.add("id", resultSet.getString("id"));
						//aFile.add("course", resultSet.getString("course"));
						//aFile.add("assignment", resultSet.getString("assignment"));
						//aFile.add("owner", resultSet.getString("owner"));
						aFile.add("dictionary", resultSet.getString("dictionary"));
						//aFile.add("data", resultSet.getString("json"));

						json.add(aFile);

						counter++;
					}

					result = json.build().toString();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				dbDriver.closeStatement(statement, resultSet);

				M_log.info("Processed " + counter + " entries");

				return (result);
			}

			// >-------------------------------------------------------------------------

			if ((controller.equalsIgnoreCase("list") == true) && (action.equalsIgnoreCase("pending") == true)) {
				String result = "[]";
				ResultSet resultSet = null;
				Statement statement = null;

				try {
					statement = dbDriver.createStatement();

					String statementString = dbDriver.selectAllPending();

					M_log.info("Executing: " + statementString);

					resultSet = statement.executeQuery(statementString);
				} catch (Exception e) {
					M_log.info("Internal error: " + e.getMessage());
					return ("[]");
				}

				try {
					JsonArrayBuilder json = Json.createArrayBuilder();

					counter = 0;

					while (resultSet.next()) {
						JsonObjectBuilder aFile = Json.createObjectBuilder();
						aFile.add("id", resultSet.getString("id"));
						//aFile.add("course", resultSet.getString("course"));
						//aFile.add("assignment", resultSet.getString("assignment"));
						//aFile.add("owner", resultSet.getString("owner"));
						//aFile.add("dictionary", dbDriver.getDictionary(resultSet.getString("assignment")));
						//aFile.add("data", resultSet.getString("json"));

						json.add(aFile);

						counter++;
					}

					result = json.build().toString();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				dbDriver.closeStatement(statement, resultSet);

				M_log.info("Processed " + counter + " entries");

				return (result);
			}

			// >-------------------------------------------------------------------------

			if (controller.equalsIgnoreCase("update") == true) {
				String rawJSON=readRequestData (request);
				String result = "[]";
				
				if (rawJSON!=null) {
					if (rawJSON.isEmpty()==false) {
						M_log.info("Processing incoming data: " + rawJSON);
		
						JsonReader jsonReader = Json.createReader(new StringReader(rawJSON));
		
						JsonArray updateList = jsonReader.readArray();
		
						M_log.info("Processing " + updateList.size() + " updates ...");
		
						for (int i = 0; i < updateList.size(); i++) {
							JsonObject fileObject = updateList.getJsonObject(i);
		
							String fileID = fileObject.getString("id");
							String fileJSON = fileObject.getString("data");
		
							M_log.info("Updating: " + fileID);
		
							dbDriver.updateEntry(fileID, fileJSON);
						}
					}
				}
				
				return (result);
			}
			
			// >-------------------------------------------------------------------------

			if (controller.equalsIgnoreCase("dictionaries") == true) {
				String result = "[]";

				ArrayList<String> dicts = dbDriver.getDictionaries();

				if (dicts != null) {
					JsonArrayBuilder json = Json.createArrayBuilder();

					counter = 0;

					for (int i = 0; i < dicts.size(); i++) {
						json.add(dicts.get(i));

						counter++;
					}

					result = json.build().toString();

					M_log.info("Processed " + counter + " entries");
				}

				return (result);
			}

			// >-------------------------------------------------------------------------

			if (controller.equalsIgnoreCase("document") == true) {
				String result = "{}";
				String fileId=request.getParameter("id");

				M_log.info("Retrieving document with id: " + fileId);
				
				OLILTIFileObject fileEntry = dbDriver.getFile(fileId);
				JsonObjectBuilder aFile = null;

				if (fileEntry != null) {
					String aDictionary=dbDriver.getDictionary(fileEntry.fAssignment);
					
					aFile = Json.createObjectBuilder();
					aFile.add("id", fileEntry.fId);
					aFile.add("name", fileEntry.fName);
					aFile.add("course", fileEntry.fCourse);
					aFile.add("assignment", fileEntry.fAssignment);
					aFile.add("dictionary", aDictionary);
					aFile.add("owner", fileEntry.fOwner);
					aFile.add("fullname", fileEntry.fFullname);
					aFile.add("state", fileEntry.fState);
					aFile.add("data", fileEntry.fData);
					
					result = aFile.build().toString();
				}

				return (result);
			}

			// >-------------------------------------------------------------------------

			if ((controller.equalsIgnoreCase("state") == true) && (action.equalsIgnoreCase("get") == true)) {
				String result = "{}";
				String fileId=request.getParameter("id");
				String fileState=request.getParameter("state");

				M_log.info("Setting state for document with id: " + fileId + ", to: " + fileState);
				
				return (result);
			}
			
			// >-------------------------------------------------------------------------

			if ((controller.equalsIgnoreCase("state") == true) && (action.equalsIgnoreCase("set_submitted") == true)) {
				String result = "{}";
				String fileId=request.getParameter("id");

				M_log.info("setting state to submitted for document with id: " + fileId);
                                
				int return_value = dbDriver.submitFile(fileId);
                                //TODO: check return value and return appropriate error.
				
				return (result);
			}

			// >-------------------------------------------------------------------------

			if ((controller.equalsIgnoreCase("tags") == true) && (action.equalsIgnoreCase("get") == true)) {
				String result = "{}";
				String fileId=request.getParameter("id");
				String fileState=request.getParameter("tags");

				M_log.info("Getting all the tags for document with id: " + fileId + ", to: " + fileState);
				
				
				
				return (result);
			}
			
			// >-------------------------------------------------------------------------

			if ((controller.equalsIgnoreCase("tags") == true) && (action.equalsIgnoreCase("set") == true)) {
				String documentId=request.getParameter("id");

				M_log.info("Setting the state and tags for document with id: " + documentId);
				
				String result = "{}";
	
				String rawJSON=readRequestData (request);
				if (rawJSON!=null) {		
					if (rawJSON.isEmpty()==false) {
						M_log.info("Processing incoming data: " + rawJSON);
						
				    JsonReader jsonReader = Json.createReader(new StringReader(rawJSON));

				    JsonArray updateList = jsonReader.readArray();
					}
				}
				
				return (result);
			}			
						
			// >-------------------------------------------------------------------------
		}

		return ("[]");
	}
}
