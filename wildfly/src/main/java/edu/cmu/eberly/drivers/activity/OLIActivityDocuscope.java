/**
 * https://stackoverflow.com/questions/7114087/html5-file-upload-to-java-servlet
 */
package edu.cmu.eberly.drivers.activity;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;
import java.util.logging.Logger;
import java.util.zip.ZipOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.http.client.ClientProtocolException;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

import edu.cmu.eberly.OLIUserVisibleException;
import edu.cmu.eberly.data.ByteBufferBackedInputStream;
import edu.cmu.eberly.data.MySQLDriverFilemanager;
import edu.cmu.eberly.data.OLILTIFileObject;
import edu.cmu.eberly.data.OLILTISession;
import edu.cmu.eberly.data.canvas.CanvasAssignmentSubmission;
import edu.cmu.eberly.data.canvas.CanvasRESTAPI;
import edu.cmu.eberly.data.document.DataReader;
import edu.cmu.eberly.data.document.DocxToPDFConverter;
import edu.cmu.eberly.drivers.OLILTIDriverInterface;
import edu.cmu.eberly.tools.FileZipOutput;
import edu.cmu.eberly.tools.JSONTools;
import edu.cmu.eberly.tools.LTILMSData;
import edu.cmu.eberly.tools.OAuthGenerator;
import edu.cmu.eberly.tools.ServletTools;
import edu.cmu.eberly.tools.StringTools;
import edu.cmu.eberly.tools.URLLoader;

/**
 * @author vvelsen
 */
public class OLIActivityDocuscope extends OLIActivityBase implements OLILTIDriverInterface {

  private static Logger M_log = Logger.getLogger(OLIActivityDocuscope.class.getName());

  protected ServletTools sTools = new ServletTools();

  private MySQLDriverFilemanager dbDriver = null;
  private CanvasRESTAPI lmsDriver = null;

  private Boolean useLTI = true;
  private Boolean useBodyHash = false;

  // protected CanvasRESTAPIInterface LMSDriver = null;

  /**
   *
   */
  public OLIActivityDocuscope() {
    this.setLTIControllerCategory("activity");
    this.setLTIControllerName("docuscope");
  }

  /**
   *
   */
  @Override
  public Boolean init() {
    dbDriver = new MySQLDriverFilemanager();
    dbDriver.init();

    String urlBase = "https://thelsamar.oli.cmu.edu";
    if (System.getProperty("LTIPreviewHost") != null) {
      urlBase = System.getProperty("LTIPreviewHost");
    }

    String token = "-0-";
    if (System.getProperty("RESToAuthToken") != null) {
      token = System.getProperty("RESToAuthToken");
    }

    lmsDriver = new CanvasRESTAPI();
    lmsDriver.setNrRequests(0L);
    lmsDriver.setAuthToken(token);
    lmsDriver.setBaseURL(urlBase);
    lmsDriver.init();

    return true;
  }

  /**
   *
   */
  @Override
  public void destroy() {
    M_log.info("destroy ()");
    dbDriver.close();
  }

  /**
   *
   */
  @Override
  public String getSecret() {
    String targetSecret = System.getProperty("LTIDuoScopeSecret");
    if (targetSecret != null) {
      return (targetSecret);
    }

    return (System.getProperty("LTIDocuscopeSecret"));
  }

  /**
   *
   */
  public String getKey() {
    String targetKey = System.getProperty("LTIDuoScopeKey");
    if (targetKey != null) {
      return (targetKey);
    }

    return (System.getProperty("LTIDocuscopeKey"));
  }

  /**
   * @Override
   */
  public String generatePage(OLILTISession session, LTILMSData lmsData, HttpServletRequest request,
      HttpServletResponse response) throws OLIUserVisibleException {
    M_log.info("generatePage ()");

    // LTILMSData lmsData = aData;

    String courseID = session.getCourseId();
    String userId = session.getUserId();

    M_log.info("Setting course id for this session to: " + courseID + ". with user id: " + userId);

    if (lmsData.isAdministrator() == true) {
      return (generatePageInstructor(session, lmsData, request, response));
    } else {
      if ((lmsData.isInstructor() == true) || (lmsData.isTA() == true)) {
        return (generatePageInstructor(session, lmsData, request, response));
      }
    }

    if (lmsData.isStudent() == true) {
      return (generatePageStudent(session, lmsData, request, response));
    }

    return ("<html>This activity was unable to determine the proper user role. Please contact technical support.</html>");
  }

  /**
   *
   */
  private void submitFile(String anId) {
    return;
  }

  /**
   * @param session
   * @param aData
   * @param request
   * @param response
   * @return
   */
  private String generatePageStudent(OLILTISession session, LTILMSData lmsData, HttpServletRequest request,
      HttpServletResponse response) {
    M_log.info("generatePageStudent ()");

    String assignmentId = lmsData.getAssignmentID();
    String userId = lmsData.getParameter("user_id");
    //String dictionary = null;
    Boolean showModels = false;

    if (dbDriver.assignmentExists(assignmentId) < 1) {
      return (sTools.loadResourceFromWAR(request.getServletContext(), "page-docuscope-student-disabled.html"));
    }

    /*
     * Boolean showStudent = dbDriver.getToggleShowStudent(assignmentId);
     * 
     * if (showStudent==false) { return
     * (sTools.loadResourceFromWAR(request.getServletContext(),
     * "page-docuscope-student-disabled.html")); }
     */

    /*dictionary = dbDriver.getDictionary(assignmentId);

    if (dictionary != null) {
      if (dictionary.isEmpty() == true) {
        return (sTools.loadResourceFromWAR(request.getServletContext(), "page-docuscope-nodictionary-student.html"));
      }
    }*/

    Boolean showStudent = dbDriver.getToggleShowStudent(assignmentId);

    String showModelsOverride = lmsData.getParameter("showModels");
    if (showModelsOverride != null) {
      showModels = StringTools.string2Boolean(showModelsOverride);
      M_log.info("We have an override for variable showModels: " + showModelsOverride + " => " + showModels);
    } else {
      showModels = dbDriver.getToggleShowModels(assignmentId);
    }

    String action = LTILMSData.getAction(request);

    if (action == null) {
      String uploadPage = "";

      /*
       * if (showModels == true) { uploadPage =
       * sTools.loadResourceFromWAR(request.getServletContext(),
       * "page-docuscope-student-with-models.html"); } else { uploadPage =
       * sTools.loadResourceFromWAR(request.getServletContext(),
       * "page-docuscope-student.html"); }
       */

      uploadPage = sTools.loadResourceFromWAR(request.getServletContext(), "page-docuscope-student.html");

      // String formatted = "";
      // formatted = generateSettings(request, session, lmsData, uploadPage);

      JsonObject json = generateSettingsObject(request, session, lmsData);

      JsonObjectBuilder builder = JSONTools.jsonObjectToBuilder(json);

      if (showModels != null) {
        builder.add("showmodels", showModels);
      } else {
        builder.add("showmodels", false);
      }

      builder.add("showstudent", showStudent);

      String formatted = uploadPage.replaceAll("/[*]SETTINGS[*]/", "var settings=" + builder.build().toString() + ";");

      return formatted;
    } else {
      M_log.info("Inspecting sub command ...");

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("list") == true) {
        M_log.info("Inspecting list command, with showModels=" + showModels);

        String result = "";

        if (showModels == false) {
          result = getJSONFileDataStudent(userId, assignmentId);
        } else {
          result = getJSONFileDataStudentWithModels(userId, assignmentId);
        }

        return (result);
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("trash") == true) {
        M_log.info("Inspecting trash command ...");

        String trash = request.getParameter("trash");

        if (trash != null) {
          String[] trashList = trash.split(",");

          for (int i = 0; i < trashList.length; i++) {
            M_log.info("Trashing : " + trashList[i]);
            removeFile(lmsData, trashList[i]);
          }
        }

        // Send the edited list of files for this user back as a response

        String result = getJSONFileDataStudent(userId, assignmentId);

        // M_log.info ("Filedata: " + result);

        return (result);
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("downloadfile") == true) {
        processFileDownload(request, response);
        return (null);
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("upload") == true) {
        // Currently handled at the top level servlet OLILTIMicroservice
      }

      // >----------------------------------------------------------------
    }

    return ("");
  }

  /**
   *
   * @param session
   * @param aData
   * @param request
   * @param response
   * @return
   */
  private String generatePageInstructor(OLILTISession session, LTILMSData lmsData, HttpServletRequest request,
      HttpServletResponse response) {
    M_log.info("generatePageInstructor ()");

    String courseID = lmsData.getParameter("context_id");
    String assignmentId = lmsData.getAssignmentID();
    String userId = lmsData.getParameter("user_id");

    String action = LTILMSData.getAction(request);

    if (action == null) {
      String uploadPage = sTools.loadResourceFromWAR(request.getServletContext(), "page-docuscope-instructor.html");

      String formatted = "";
      String dictionary = "";
      Boolean showModels = false;
      Boolean showStudent = false;

      // >---------------------------------------------------------------

      String assignmentName = "Unassigned";
      if (lmsData.getParameter("custom_canvas_assignment_title").isEmpty() == false) {
        assignmentName = lmsData.getParameter("custom_canvas_assignment_title");
      }

      String courseName = "Unassigned";
      if (lmsData.getParameter("context_title").isEmpty() == false) {
        courseName = lmsData.getParameter("context_title");
      }

      String instructorName = "Unassigned";
      if (lmsData.getParameter("lis_person_name_full") != null) {
        if (lmsData.getParameter("lis_person_name_full").isEmpty() == false) {
          instructorName = lmsData.getParameter("lis_person_name_full");
        }
      }

      if (dbDriver.assignmentExists(assignmentId) < 1) {
        M_log.info("Assignment does not exist, creating ...");
        dbDriver.createAssignment(assignmentId, "101_Proposal", assignmentName, courseName, instructorName);
        // Make sure we have a dictionary
        // dbDriver.updateAssignmentDictionary(assignmentId, "101_Proposal",
        // assignmentName, courseName, instructorName);
      } else {
        M_log.info("Assignment exists, we're good");
      }

      // >----------------------------------------------------------------

      dictionary = dbDriver.getDictionary(assignmentId);
      showModels = dbDriver.getToggleShowModels(assignmentId);
      showStudent = dbDriver.getToggleShowStudent(assignmentId);

      JsonObject json = generateSettingsObject(request, session, lmsData);

      JsonObjectBuilder builder = JSONTools.jsonObjectToBuilder(json);

      if (dictionary != null) {
        builder.add("dictionary", dictionary);
      } else {
        builder.add("dictionary", "");
      }

      if (showModels != null) {
        builder.add("showmodels", showModels);
      } else {
        builder.add("showmodels", false);
      }

      if (showStudent != null) {
        builder.add("showstudent", showStudent);
      } else {
        builder.add("showstudent", false);
      }

      formatted = uploadPage.replaceAll("/[*]SETTINGS[*]/", "var settings=" + builder.build().toString() + ";");

      formatted = generateDictionaryTag(formatted, assignmentId);

      return formatted;
    } else {
      M_log.info("Inspecting sub command: " + action);

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("selectdictionary") == true) {
        M_log.info("Inspecting selectedictionary command ...");

        String chosenDictionary = lmsData.getParameter("selecteddictionary");

        M_log.info("Setting dictionary for assignment " + assignmentId + " to: " + chosenDictionary);

        String assignmentName = "Unassigned";
        if (lmsData.getParameter("custom_canvas_assignment_title").isEmpty() == false) {
          assignmentName = lmsData.getParameter("custom_canvas_assignment_title");
        }

        String courseName = "Unassigned";
        if (lmsData.getParameter("context_title").isEmpty() == false) {
          courseName = lmsData.getParameter("context_title");
        }

        String instructorName = "Unassigned";
        if (lmsData.getParameter("lis_person_name_full") != null) {
          if (lmsData.getParameter("lis_person_name_full").isEmpty() == false) {
            instructorName = lmsData.getParameter("lis_person_name_full");
          }
        }

        dbDriver.updateAssignmentDictionary(assignmentId, chosenDictionary, assignmentName, courseName, instructorName);

        // If the dictionary changes for an assignment then the back-end will need to
        // re-tag everything
        dbDriver.resetAssignmentTags(assignmentId);

        return (chosenDictionary);
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("list") == true) {
        M_log.info("Inspecting list command ...");

        String result = getJSONFileDataInstructor("-1", assignmentId);

        return (result);
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("analyze") == true) {
        M_log.info("Inspecting analyze command ...");

        return ("<html><body>Docuscope Analsysis ...</body></html>");
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("trash") == true) {
        M_log.info("Inspecting trash command ...");

        String trash = request.getParameter("trash");

        if (trash != null) {
          String[] trashList = trash.split(",");

          for (int i = 0; i < trashList.length; i++) {
            M_log.info("Trashing : " + trashList[i]);
            removeFile(lmsData, trashList[i]);
          }
        }

        // Send the edited list of files for this user back as a response
        String result = getJSONFileDataInstructor(userId, assignmentId);

        return (result);
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("upload") == true) {
        // Currently handled at the top level servlet OLILTIMicroservice
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("submitselected") == true) {
        M_log.info("Submitting selected documents for analysis ...");

        String trash = request.getParameter("submit");

        if (trash != null) {
          String[] trashList = trash.split(",");

          for (int i = 0; i < trashList.length; i++) {
            M_log.info("Trashing : " + trashList[i]);
            submitFile(trashList[i]);
          }
        }

        return ("[]");
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("downloadfile") == true) {
        processFileDownload(request, response);
        return null;
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("downloadlist") == true) {
        String tempFolderPath = System.getProperty("jboss.server.temp.dir");

        String downloadIds = request.getParameter("ids");

        if (downloadIds != null) {
          if (downloadIds.isEmpty() == false) {
            String[] fileIds = downloadIds.split(",");

            M_log.info("Generating zip file from " + fileIds.length + " files in folder: " + tempFolderPath);

            FileZipOutput zipper = new FileZipOutput();

            FileOutputStream fos = null;

            String tempFilename = ("DocuScope-" + StringTools.generateStringUUID() + ".zip");

            try {
              fos = new FileOutputStream(tempFolderPath + "/" + tempFilename);
            } catch (FileNotFoundException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
              return ("[]");
            }

            ZipOutputStream zipOS = new ZipOutputStream(fos);

            for (int k = 0; k < fileIds.length; k++) {
              M_log.info("Adding file with id " + fileIds[k] + " to zip archive");

              OLILTIFileObject fileObject = dbDriver.getFileBinary(fileIds[k]);

              if (fileObject != null) {
                try {
                  // At this point we have a problem with the database. The user is allowed
                  // to-reupload
                  // a file to the db, which means that you might have more than one copy of the
                  // same
                  // file in the zip file, which is not allowed. For now we're adding a unique
                  // index
                  // to each file name just in case.
                  zipper.writeToZipFile(fileObject.raw, k + "-" + fileObject.fName, zipOS);
                } catch (FileNotFoundException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
                } catch (IOException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
                }
              }
            }

            String tempZip = tempFolderPath + "/" + tempFilename;

            M_log.info("Writing archive to: " + tempZip);

            if (zipper.write(tempZip, zipOS) == true) {
              // Now download to browser ...
              if (writeFileToResponse(response, tempZip, tempFilename) == true) {
                // Delete temp file ...

                File file = new File(tempZip);

                if (file.delete()) {
                  M_log.info("File deleted successfully: " + tempZip);
                } else {
                  M_log.info("Failed to delete the file: " + tempZip);
                }

                // Indicate to the wrapper that we've taking care of the request ourselves by
                // returning null
                return (null);
              }
            } else {
              M_log.info("Error writing archive");
            }
          }
        }

        return ("[]");
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("preview") == true) {
        String uploadPage = sTools.loadResourceFromWAR(request.getServletContext(), "page-docuscope-preview.html");

        M_log.info("Finding PDF preview for user " + userId + ", in course: " + courseID + " and in assignment: "
            + assignmentId);

        JsonObject json = generateSettingsObject(request, session, lmsData);

        String formatted = uploadPage.replaceAll("/[*]SETTINGS[*]/", "var settings=" + json.toString() + ";");

        formatted = formatted.replaceAll("/[*]PDFDOCUMENT[*]/",
            "/lti/activity/docuscope/document?course=" + courseID + "&assignment=" + assignmentId + "&user=" + userId
                + "&token=" + session.getToken() + "&ext_lti_assignment_id=" + request.getParameter("resource_link_id")
                + "&roles=" + request.getParameter("roles") + "&student_id=" + request.getParameter("student_id")
                + "&resource_link_id_override=" + request.getParameter("resource_link_id_override"));

        return formatted;
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("document") == true) {
        dbDriver.loadPreview(request.getParameter("course"), request.getParameter("student_id"),
            request.getParameter("resource_link_id_override"), response);
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("togglemodels") == true) {
        M_log.info("Inspecting togglemodels command ...");

        String toggled = lmsData.getParameter("toggle");

        M_log.info("Toggling: " + toggled + " assignment " + assignmentId);

        if (StringTools.string2Boolean(toggled) == true) {
          dbDriver.toggleAssignmentShowModel(assignmentId, "true");
        } else {
          dbDriver.toggleAssignmentShowModel(assignmentId, "false");
        }
      }

      // >----------------------------------------------------------------

      if (action.equalsIgnoreCase("togglestudent") == true) {
        M_log.info("Inspecting togglestudent command ...");

        String toggled = lmsData.getParameter("toggle");

        M_log.info("Toggling: " + toggled + " assignment " + assignmentId);

        if (StringTools.string2Boolean(toggled) == true) {
          dbDriver.toggleAssignmentShowStudent(assignmentId, "true");
        } else {
          dbDriver.toggleAssignmentShowStudent(assignmentId, "false");
        }
      }

      // >----------------------------------------------------------------
    }

    return ("");
  }

  /**
   * @param request
   * @param response
   */
  private void processFileDownload(HttpServletRequest request, HttpServletResponse response) {
    M_log.info("processFileDownload ()");

    String fileId = request.getParameter("id");

    if (fileId != null) {
      OLILTIFileObject fileData = dbDriver.getFileBinary(fileId);
      if (fileData == null) {
        return;
      }

      // String jsonData = "";
      String filename = fileData.fName;

      // jsonData = fileData.fData;

      M_log.info("File data retrieved from file: " + filename);

      if (fileData.raw != null) {
        M_log.info("Writing data to browser ...");

        writeDataToBrowser(response, filename, fileData.raw);
      }
    }
  }

  /**
   * @param aPage
   * @param assignmentID
   * @return
   */
  private String generateDictionaryTag(String aPage, String assignmentID) {
    M_log.info("generateDictionaryTag ()");

    ArrayList<String> resultSet = null;

    StringBuffer dictionaryHTML = new StringBuffer();
    dictionaryHTML.append("<select id=\"dictionaries\">");
    dictionaryHTML.append("<option value=\"---\">---</option>");

    resultSet = dbDriver.getDictionaries();

    if (resultSet!=null) {
      for (int i = 0; i < resultSet.size(); i++) {
        String dictionaryEntry = resultSet.get(i);
  
        dictionaryHTML.append("<option value=\"" + dictionaryEntry + "\">" + dictionaryEntry + "</option>");
      }
    }

    dictionaryHTML.append("</select>");

    String formatted = aPage.replaceAll("  <!--DICTIONARIES-->", dictionaryHTML.toString());

    return (formatted);
  }

  /**
   * @return
   */
  private String getJSONFileDataStudent(String anId, String anAssignmentId) {
    M_log.info("getJSONFileDataStudent (" + anId + "," + anAssignmentId + ")");

    if (dbDriver != null) {
      String result = "[]";
      ResultSet resultSet = null;
      Statement statement = null;

      try {
        statement = dbDriver.createStatement();

        String statementString = dbDriver.selectAllStudent(anId, anAssignmentId);

        M_log.info("Executing: " + statementString);

        resultSet = statement.executeQuery(statementString);
      } catch (Exception e) {
        M_log.info("Internal error: " + e.getMessage());
        return ("[]");
      }

      if (resultSet == null) {
        dbDriver.closeStatement(statement, resultSet);
        return ("[]");
      }

      try {
        JsonArrayBuilder json = Json.createArrayBuilder();

        while (resultSet.next()) {
          JsonObjectBuilder aFile = Json.createObjectBuilder();

          String rawFilename = resultSet.getString("name");
          String formattedName = rawFilename;

          if (rawFilename.length() > 24) {
            formattedName = (rawFilename.substring(0, 24) + "...");
          }

          SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

          aFile.add("id", resultSet.getString("id"));
          aFile.add("name", formattedName);
          aFile.add("type", resultSet.getString("type"));
          aFile.add("created", iso.format(resultSet.getDate("created")));
          aFile.add("createdraw", resultSet.getString("createdraw"));
          aFile.add("size", resultSet.getInt("size"));
          aFile.add("course", resultSet.getString("course"));
          aFile.add("owner", resultSet.getString("owner"));
          aFile.add("state", resultSet.getString("state"));
          aFile.add("ownedby", resultSet.getString("ownedby"));
          aFile.add("fullname", resultSet.getString("fullname"));

          json.add(aFile);
        }

        result = json.build().toString();
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      dbDriver.closeStatement(statement, resultSet);

      return (result);
    }

    return "[]";
  }

  /**
   * @return
   */
  private String getJSONFileDataStudentWithModels(String anId, String anAssignmentId) {
    M_log.info("getJSONFileDataStudentWithModels (" + anId + "," + anAssignmentId + ")");

    if (dbDriver != null) {
      String result = "[]";
      ResultSet resultSet = null;
      Statement statement = null;

      try {
        JsonArrayBuilder json = Json.createArrayBuilder();

        try {
          statement = dbDriver.createStatement();

          String statementString = dbDriver.selectAllStudent(anId, anAssignmentId);

          M_log.info("Executing: " + statementString);

          resultSet = statement.executeQuery(statementString);
        } catch (Exception e) {
          M_log.info("Internal error: " + e.getMessage());
          return ("[]");
        }

        if (resultSet == null) {
          M_log.info ("Nothing retrieved, bump");
          dbDriver.closeStatement(statement, resultSet);
          return ("[]");
        }

        while (resultSet.next()) {
          JsonObjectBuilder aFile = Json.createObjectBuilder();

          String rawFilename = resultSet.getString("name");
          
          //M_log.info("Adding student file: " + rawFilename);
          
          String formattedName = rawFilename;

          if (rawFilename.length() > 24) {
            formattedName = (rawFilename.substring(0, 24) + "...");
          }

          SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

          aFile.add("id", resultSet.getString("id"));
          aFile.add("name", formattedName);
          aFile.add("type", resultSet.getString("type"));
          aFile.add("created", iso.format(resultSet.getDate("created")));
          aFile.add("createdraw", resultSet.getString("createdraw"));
          aFile.add("size", resultSet.getInt("size"));
          aFile.add("course", resultSet.getString("course"));
          aFile.add("owner", resultSet.getString("owner"));
          aFile.add("state", resultSet.getString("state"));
          aFile.add("ownedby", resultSet.getString("ownedby"));
          aFile.add("fullname", resultSet.getString("fullname"));

          json.add(aFile);
        }

        dbDriver.closeStatement(statement, resultSet);

        // >---------------------------------------------------------------------------

        statement = dbDriver.createStatement();

        if (statement != null) {
          String statementString = dbDriver.selectAllInstructor("-1", anAssignmentId);

          M_log.info("Executing: " + statementString);

          try {
            resultSet = statement.executeQuery(statementString);
          } catch (Exception e) {
            M_log.info("Internal error: " + e.getMessage());
            return ("[]");
          }
        } else {
          M_log.info("Error, unable to create new statement");
        }

        M_log.info("Here");
        
        try {
          while (resultSet.next()) {
            String rawFilename = resultSet.getString("name");

            //M_log.info("Adding instructor file: " + rawFilename);

            String formattedName = rawFilename;

            if (rawFilename.length() > 24) {
              formattedName = (rawFilename.substring(0, 24) + "...");
            }

            SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

            JsonObjectBuilder aFile = Json.createObjectBuilder();
            aFile.add("id", resultSet.getString("id"));
            aFile.add("name", formattedName);
            aFile.add("type", resultSet.getString("type"));
            aFile.add("created", iso.format(resultSet.getDate("created")));
            aFile.add("createdraw", resultSet.getString("createdraw"));
            aFile.add("size", resultSet.getInt("size"));
            aFile.add("course", resultSet.getString("course"));
            aFile.add("owner", resultSet.getString("owner"));
            aFile.add("state", resultSet.getString("state"));
            aFile.add("ownedby", resultSet.getString("ownedby"));
            aFile.add("fullname", resultSet.getString("fullname"));

            json.add(aFile);
          }
        } catch (SQLException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }

        dbDriver.closeStatement(statement, resultSet);

        // >---------------------------------------------------------------------------

        result = json.build().toString();
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      return (result);
    }
    
    M_log.info("Bottoming out ...");

    return "[]";
  }

  /**
   * @return
   */
  private String getJSONFileDataInstructor(String anId, String anAssignmentId) {
    M_log.info("getJSONFileDataInstructor ()");

    if (dbDriver != null) {
      String result = "[]";
      ResultSet resultSet = null;
      Statement statement = null;
      JsonArrayBuilder json = Json.createArrayBuilder();

      // >---------------------------------------------------------------------------

      try {
        statement = dbDriver.createStatement();

        String statementString = dbDriver.selectAllStudent(anId, anAssignmentId);

        M_log.info("Executing: " + statementString);

        resultSet = statement.executeQuery(statementString);
      } catch (Exception e) {
        M_log.info("Internal error: " + e.getMessage());
        return ("[]");
      }

      if (resultSet == null) {
        dbDriver.closeStatement(statement, resultSet);
        return ("[]");
      }

      // Temporary map so that we can figure out the last submitted file per student
      Hashtable<String, JsonObject> fileMapper = new Hashtable<String, JsonObject>();

      int index = 0;

      try {
        while (resultSet.next()) {
          JsonObject testJson = ResultSetToJson(resultSet);

          String testOwner = testJson.getString("owner");

          M_log.info("Processing file with owner id: " + testOwner);

          if (index == 0) {
            M_log.info("Adding file straight up");
            fileMapper.put(testOwner, testJson);
          } else {
            JsonObject existingSet = fileMapper.get(testOwner);

            if (existingSet == null) {
              M_log.info("Error: Unable to retrieve resultset for owner: " + testOwner + ", assuming new file ...");
              fileMapper.put(testOwner, testJson);
            } else {
              M_log.info("Success!");
              double original = Double.parseDouble(existingSet.getString("createdraw"));
              double newstamp = Double.parseDouble(testJson.getString("createdraw"));

              if (newstamp > original) {
                // Overwrite file with newer entry
                fileMapper.put(testOwner, testJson);
              }
            }
          }

          M_log.info("Processed file with index: " + index);

          index++;
        }
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      Set<String> keys = fileMapper.keySet();
      for (String key : keys) {
        JsonObject derivedSet = fileMapper.get(key);
        JsonObjectBuilder aFile = JSONTools.jsonObjectToBuilder(derivedSet);
        aFile.add("id", derivedSet.getString("id"));
        aFile.add("name", derivedSet.getString("name"));
        aFile.add("type", derivedSet.getString("type"));
        aFile.add("created", derivedSet.getString("created"));
        aFile.add("createdraw", derivedSet.getString("createdraw"));
        aFile.add("size", derivedSet.getString("size"));
        aFile.add("course", derivedSet.getString("course"));
        aFile.add("owner", derivedSet.getString("owner"));
        aFile.add("state", derivedSet.getString("state"));
        aFile.add("ownedby", derivedSet.getString("ownedby"));
        aFile.add("fullname", derivedSet.getString("fullname"));

        json.add(aFile);
      }

      // >---------------------------------------------------------------------------

      statement = dbDriver.createStatement();

      if (statement != null) {
        String statementString = dbDriver.selectAllInstructor(anId, anAssignmentId);

        M_log.info("Executing: " + statementString);

        try {
          resultSet = statement.executeQuery(statementString);
        } catch (Exception e) {
          M_log.info("Internal error: " + e.getMessage());
          return ("[]");
        }
      }

      try {
        while (resultSet.next()) {
          String rawFilename = resultSet.getString("name");

          // M_log.info("Checking filename: " + rawFilename + " with length: " +
          // rawFilename.length());

          String formattedName = rawFilename;

          if (rawFilename.length() > 24) {
            formattedName = (rawFilename.substring(0, 24) + "...");
          }

          SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

          JsonObjectBuilder aFile = Json.createObjectBuilder();
          aFile.add("id", resultSet.getString("id"));
          aFile.add("name", formattedName);
          aFile.add("type", resultSet.getString("type"));
          aFile.add("created", iso.format(resultSet.getDate("created")));
          aFile.add("createdraw", resultSet.getString("createdraw"));
          aFile.add("size", resultSet.getInt("size"));
          aFile.add("course", resultSet.getString("course"));
          aFile.add("owner", resultSet.getString("owner"));
          aFile.add("state", resultSet.getString("state"));
          aFile.add("ownedby", resultSet.getString("ownedby"));
          aFile.add("fullname", resultSet.getString("fullname"));

          json.add(aFile);
        }

        result = json.build().toString();
      } catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      dbDriver.closeStatement(statement, resultSet);

      // >---------------------------------------------------------------------------

      return (result);
    }

    return "[]";
  }

  /**
   * @param aSet
   * @return
   */
  private JsonObject ResultSetToJson(ResultSet aSet) {
    try {
      String rawFilename = aSet.getString("name");

      M_log.info("Checking filename: " + rawFilename + " with length: " + rawFilename.length());

      String formattedName = rawFilename;

      if (rawFilename.length() > 24) {
        formattedName = (rawFilename.substring(0, 24) + "...");
      }

      JsonObjectBuilder aFile = Json.createObjectBuilder();
      SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
      aFile.add("id", aSet.getString("id"));
      aFile.add("name", formattedName);
      aFile.add("type", aSet.getString("type"));
      aFile.add("created", iso.format(aSet.getDate("created")));
      aFile.add("createdraw", aSet.getString("createdraw"));
      aFile.add("size", aSet.getString("size"));
      aFile.add("course", aSet.getString("course"));
      aFile.add("owner", aSet.getString("owner"));
      aFile.add("state", aSet.getString("state"));
      aFile.add("ownedby", aSet.getString("ownedby"));
      aFile.add("fullname", aSet.getString("fullname"));

      return (aFile.build());
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return (null);
  }

  /**
   * @param request
   * @return
   */
  public String processFileUpload(HttpServletRequest request, LTILMSData lmsData) {
    M_log.info("processFileUpload ()");

    Part file = null;

    try {
      file = request.getPart("file");
    } catch (IOException e) {
      return ("Error uploading file IOException: " + e.getMessage());
    } catch (ServletException e) {
      return ("Error uploading file ServletException: " + e.getMessage());
    }

    String ownedby = request.getParameter("ownedby");
    if (ownedby.equals("1")) {
      ownedby = "instructor";
    }

    if (ownedby.equals("0")) {
      ownedby = "student";
    }

    String context_id = request.getParameter("context_id");
    String user_id = request.getParameter("user_id");
    String coursename = request.getParameter("coursename");
    String studentnamefull = request.getParameter("studentnamefull");
    String assignment = request.getParameter("resource_link_id");
    String timezone = request.getParameter("custom_client_timezone");
    String incomingFiledate = request.getParameter("created");
    String incomingFiledateraw = request.getParameter("createdraw");

    if (timezone == null) {
      timezone = request.getParameter("timezone");
      if (timezone == null) {
        timezone = "GMT";
      }
    }

    M_log.info("Using timezone: " + timezone);

    String incomingFilename = getFilename(file);
    Boolean convert = false;

    if ((incomingFilename.contains(".pdf") == true) || (incomingFilename.contains(".PDF") == true)) {
      M_log.info("Detected PDF upload, converting ...");
      convert = true;
      return "[]";
    }

    M_log.info("Found extra data, context_id: " + context_id + ", user_id: " + user_id + ", coursename: " + coursename
        + ", studentnamefull: " + studentnamefull + ", assignment: " + assignment + ", timezone: " + timezone);

    if (file != null) {
      M_log.info("Processing file upload ...");

      String filename = StringTools.makeFilenameDBSafe(getFilename(file));

      // >-----------------------------------------------------------------------

      byte[] byteBuffer = DataReader.readFromPart(file);

      // >-----------------------------------------------------------------------

      /*
       * String date = new SimpleDateFormat("EEEEE MMMMM yyyy HH:mm:ss").format(new
       * Date()); Long datetrans = System.currentTimeMillis(); String dateraw =
       * datetrans.toString();
       */

      OLILTIFileObject newFile = new OLILTIFileObject();

      Integer fSize = byteBuffer.length;
      newFile.fSize = fSize.toString();
      newFile.fName = filename;
      newFile.fAssignment = assignment;
      newFile.fOwner = user_id;
      newFile.fTimezone = timezone;
      newFile.fCourse = coursename;
      newFile.fFullname = studentnamefull;
      newFile.fCreated = incomingFiledate;
      newFile.fCreatedRaw = incomingFiledateraw;
      newFile.fOwnedBy = ownedby;
      newFile.raw = byteBuffer;

      if (convert == false) {
        // newFile.fData = Base64.getEncoder().encodeToString(byteBuffer);

        if (dbDriver.storeFileDocX(newFile) != -1) {
          M_log.info("File uploaded, storing copy ...");

          String result = "File [" + newFile.fId + "] " + newFile.fName + ", " + newFile.fSize + " (bytes), "
              + newFile.fOwner + ", " + newFile.fCreated + " successfully uploaded";

          savePDFCopy(new ByteBufferBackedInputStream(byteBuffer), newFile.fId);

          // Pass back a dummy grading XML block just so that we can tell canvas where
          // the preview URL is for the file in SpeedGrader
          if (lmsData.isStudent() == true) {
            informCanvas(request, lmsData, assignment, user_id, "1.0");
          } else {
            M_log.info("Upload is not by student, not generating grade passback request");
          }

          return (result);
        } else {
          M_log.info("Internal error: unable to upload file!");
        }
      } else {
        if (dbDriver.storeFilePDF(newFile) != -1) {
          String result = "File [" + newFile.fId + "] " + newFile.fName + ", " + newFile.fSize + " (bytes), "
              + newFile.fOwner + ", " + newFile.fCreated + " successfully uploaded";

          saveDocXCopy(new ByteBufferBackedInputStream(byteBuffer), newFile.fId);

          // Pass back a dummy grading XML block just so that we can tell canvas where
          // the preview URL is for the file in SpeedGrader
          if (lmsData.isStudent() == true) {
            informCanvas(request, lmsData, assignment, user_id, "1.0");
          } else {
            M_log.info("Upload is not by student, not generating grade passback request");
          }

          return (result);
        }
      }
    }

    return ("Error uploading file: file object is null");
  }

  /**
   * @param buffer
   * @param context_id
   * @param user_id
   * @param assignment
   */
  private void savePDFCopy(ByteBufferBackedInputStream buffer, String fileId) {
    M_log.info("savePDFCopy (" + fileId + ")");

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    M_log.info("Creating converter ...");

    DocxToPDFConverter converter = new DocxToPDFConverter(buffer, out, false, false);

    M_log.info("Converting ...");

    try {
      converter.convert();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return;
    }

    M_log.info("PDF encoded (allegedly), encoding to base64 ...");

    byte[] rawData = out.toByteArray();

    M_log.info("Resulting raw pdf byte array size: " + rawData.length);

    if (rawData.length == 0) {
      M_log.info("Error: raw PDF data is length 0");
      return;
    }

    if (dbDriver != null) {
      dbDriver.storePDF(rawData, fileId);
    } else {
      M_log.info("Internal error: weird, dbDriver is null now");
      return;
    }
  }

  /**
   * https://www.baeldung.com/pdf-conversions-java
   *
   * @param buffer
   * @param context_id
   * @param user_id
   * @param assignment
   */
  private void saveDocXCopy(ByteBufferBackedInputStream buffer, String fileId) {
    M_log.info("saveDocXCopy (" + fileId + ")");

    M_log.info("Creating converter ...");

    File tempFile = null;

    try {
      tempFile = File.createTempFile("prefix-", "-suffix");
    } catch (IOException e) {
      return;
    }

    tempFile.deleteOnExit();

    XWPFDocument doc = new XWPFDocument();
    PdfReader reader = null;

    try {
      reader = new PdfReader(buffer);
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }

    PdfReaderContentParser parser = new PdfReaderContentParser(reader);

    for (int i = 1; i <= reader.getNumberOfPages(); i++) {
      TextExtractionStrategy strategy = null;
      try {
        strategy = parser.processContent(i, new SimpleTextExtractionStrategy());
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        return;
      }
      String text = strategy.getResultantText();
      XWPFParagraph p = doc.createParagraph();
      XWPFRun run = p.createRun();
      run.setText(text);
      run.addBreak(BreakType.PAGE);
    }

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
      doc.write(baos);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return;
    }

    byte[] converter = baos.toByteArray();

    // String encoded = Base64.getEncoder().encodeToString(converter);

    if (dbDriver != null) {
      dbDriver.storeDocX(converter, fileId);
    } else {
      M_log.info("Internal error: weird, dbDriver is null now");
      return;
    }
  }

  /**
   *
   */
  private void removeFile(LTILMSData lmsData, String anId) {
    M_log.info("removeFile ()");

    Hashtable<String, OLILTIFileObject> files = lmsData.getFiles();
    files.remove(anId);

    // Modify DB ...
    if (dbDriver != null) {
      dbDriver.removeFile(anId);
    } else {
      M_log.info("Internal error: weird, dbDriver is null now");
    }
  }

  /**
   * References: https://canvas.instructure.com/doc/api/file.assignment_tools.html
   */
  private void informCanvas(HttpServletRequest request, LTILMSData lmsData, String sourceId, String aStudentId,
      String aResult) {
    M_log.info("informCanvas ()");

    if (useLTI) {
      informCanvasLTI(request, lmsData, sourceId, aStudentId, aResult);
    } else {
      informCanvasAPI(request, lmsData, sourceId, aStudentId, aResult);
    }
  }

  /**
   * References: https://canvas.instructure.com/doc/api/file.assignment_tools.html
   */
  private void informCanvasAPI(HttpServletRequest request, LTILMSData lmsData, String sourceId, String aStudentId,
      String aResult) {
    M_log.info("informCanvasAPI ()");

    String courseID = lmsData.getParameter("custom_canvas_course_id");
    String assignmentId = lmsData.getParameter("custom_canvas_assignment_id");
    String userID = lmsData.getParameter("custom_canvas_user_id");
    String urlBase = "https://thelsamar.oli.cmu.edu";

    if (System.getProperty("LTIPreviewHost") != null) {
      urlBase = System.getProperty("LTIPreviewHost");
    }

    String APIURL = "/api/v1/courses/" + courseID + "/assignments/" + assignmentId + "/submissions";
    String queryString = "student_id=" + aStudentId + "&resource_link_id_override="
        + lmsData.getParameter("resource_link_id");
    String url = urlBase + "/lti/activity/docuscope/preview?" + queryString;
    String urlEncoded = "";
    try {
      urlEncoded = URLEncoder.encode(url, "UTF-8");
    } catch (UnsupportedEncodingException e1) {
      e1.printStackTrace();
    }

    M_log.info("Assignment submission URL (API)" + APIURL);
    M_log.info("Assignment submission URL (Docuscope)" + url);

    CanvasAssignmentSubmission submissionShim = new CanvasAssignmentSubmission();
    submissionShim.assignment_id = assignmentId;
    submissionShim.user_id = userID;
    submissionShim.grade = "A";
    submissionShim.score = 1.0;
    submissionShim.preview_url = urlEncoded;

    lmsDriver.setBaseURL("https://cmu.test.instructure.com");
    lmsDriver.makePOSTRequestObject(APIURL, submissionShim.getJSONObject());
  }

  /**
   * http://www.imsglobal.org/specs/ltiv1p1/implementation-guide#toc-6
   * 
   * Via: Canvas Production Release Notes (2015-10-10)
   * 
   * Grade Passback
   * 
   * LTI tools that support grade passback correctly handle complete/incomplete
   * assignment types. A score of 1.0 will be marked as complete and anything less
   * than 1.0 will be marked incomplete.
   * 
   * Canvas sends an extension parameter for assignment launches that allows the
   * tool provider to pass back a raw score value instead of a percentage. The key
   * is ext_outcome_result_total_score_accepted and the value is true. The added
   * launch parameter will look like this:
   * 
   * ext_outcome_result_total_score_accepted=true
   * 
   * ext_ims_lis_basic_outcome_url vs lis_outcome_service_url ?
   * 
   * References: https://canvas.instructure.com/doc/api/file.assignment_tools.html
   */
  private void informCanvasLTI(HttpServletRequest request, LTILMSData lmsData, String sourceId, String aStudentId, String aResult) {
    M_log.info("informCanvasLTI ()");

    String token = StringTools.generateStringUUID();

    String queryString = "student_id=" + aStudentId + "&resource_link_id_override=" + lmsData.getParameter("resource_link_id");

    String urlBase = "https://thelsamar.oli.cmu.edu";
    if (System.getProperty("LTIPreviewHost") != null) {
      urlBase = System.getProperty("LTIPreviewHost");
    }
    
    String url = urlBase + "/lti/activity/docuscope/preview?" + queryString;

    String urlEncoded = "<![CDATA[" + url + "]]>";
    
    /*
    String urlEncoded = "";
    try {
      urlEncoded = URLEncoder.encode(url, "UTF-8");
    } catch (UnsupportedEncodingException e1) {
      e1.printStackTrace();
    }
    */
    
    String xmlResult = sTools.loadResourceFromWAR(request.getServletContext(), "gradepassback.xml");
    
    String ltiResponseId=lmsData.getParameter("lis_result_sourcedid");
    //String ltiResponseId=lmsData.getParameter("ext_lti_assignment_id");
    //String ltiResponseId=lmsData.getParameter("context_id");
    //String ltiResponseId=lmsData.getParameter("resource_link_id");

    xmlResult = xmlResult.replaceAll("dtransactionid", token);
    xmlResult = xmlResult.replaceAll("dsource", ltiResponseId);
    xmlResult = xmlResult.replaceAll("dgrade", aResult);
    xmlResult = xmlResult.replaceAll("dpreview", urlEncoded);
    //xmlResult = xmlResult.replaceAll("dpreview", "");
    
    //xmlResult = xmlResult.replace("\n", "").replace("\r", "");

    // M_log.info("Sending to LMS: " + xmlResult);

    String signedURL = "";
    String ltiURL = lmsData.getParameter("lis_outcome_service_url");
    
    //ltiURL="http://localhost:3030";
    //ltiURL="https://thelsamar.oli.cmu.edu/api/lti/v1/tools/6691/grade_passback";

    if (useBodyHash == false) {
      signedURL = OAuthGenerator.getSignedURL(ltiURL, getKey(), getSecret(), xmlResult);
    } else {
      signedURL = OAuthGenerator.getOAuthDataURL(ltiURL, getKey(), getSecret(), xmlResult);
    }

    URLLoader loader = new URLLoader();
    String result = "";

    try {
      result = loader.POSTRequestSignedURL(signedURL, xmlResult);
    } catch (ClientProtocolException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    if (result != null) {
      if (result.trim().indexOf("<?xml") != 0) {
        M_log.info("The LTI consumer did not send back XML, most likely responded with the default error html page");
        //M_log.info(result);
      } else {
        M_log.info("Grade passback call result: " + result);
      }
    } else {
      M_log.info("Error: POSTRequest returned null");
    }
  }
}
