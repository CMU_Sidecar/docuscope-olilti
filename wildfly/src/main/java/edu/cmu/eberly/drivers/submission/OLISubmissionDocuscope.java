package edu.cmu.eberly.drivers.submission;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.cmu.eberly.OLIUserVisibleException;
import edu.cmu.eberly.data.OLILTISession;
import edu.cmu.eberly.drivers.OLILTIDriverInterface;
import edu.cmu.eberly.drivers.activity.OLIActivityBase;
import edu.cmu.eberly.tools.LTILMSData;
import edu.cmu.eberly.tools.ServletTools;

/**
 * @author vvelsen
 */
public class OLISubmissionDocuscope extends OLIActivityBase implements OLILTIDriverInterface {

	private static Logger M_log = Logger.getLogger(OLISubmissionDocuscope.class.getName());

	protected ServletTools sTools = new ServletTools();

	private LTILMSData lmsData = null;
	
	/**
	 * 
	 */
	public OLISubmissionDocuscope () {
		this.setLTIControllerCategory("homework");
		this.setLTIControllerName("docuscope");
	}
		
	/**
	 * 
	 */
	@Override
	public Boolean init() {
		return true;
	}
	
	/**
	 * 
	 */
	@Override
	public String getSecret() {
		return (System.getProperty("LTIDocuscopeSubmissionSecret"));
	}	
	
	/**
	 * @Override
	 */
	public String generatePage(OLILTISession session, LTILMSData aData, HttpServletRequest request, HttpServletResponse response) throws OLIUserVisibleException {
		M_log.info("generatePage ()");

		lmsData = aData;
		
		String courseID = lmsData.getParameter("context_id");
		String userId = lmsData.getParameter("user_id");

		M_log.info("Setting course id for this session to: " + courseID + ". with user id: " + userId);

		if (lmsData.isInstructor()==true) {
			
		}
		
		if (lmsData.isStudent()==true) {
			
		}		
		
		//String action = LTILMSData.getAction(request);

	  String uploadPage = sTools.loadResourceFromWAR(request.getServletContext(), "page-docuscope-submission.html");

		String formatted = "";

		formatted = generateSettings(request, session, aData, uploadPage);

		return (formatted);
	}
}
