/* global settings, $ */

var remainingUpload = 0;
var totalUpload = 0;
var debugging = true;
var updateTimerId = -1;

const CLASSROOM = new URL("https://docuscope.eberly.cmu.edu/classroom/");
const DOCUSCOPE_LTI = "/lti/activity/docuscope";

/**
 *
 */
function docdebug(aMessage) {
  if (debugging == true) {
    console.log(aMessage);
  }
}

/**
 * Test if the given state is "tagged".
 * @arg state {str}
 * @returns {bool}
 */
function isTagged(state) {
  return state.trim().toLowerCase() === "tagged";
}
/**
 * Test if the given state is "error".
 * @arg state {str}
 * @returns {bool}
 */
function isError(state) {
  return state.trim().toLowerCase() === "error";
}
/**
 * Test if the given state is "tagged" or "error".
 * @arg state {str}
 * @returns {bool}
 */
function isActive(state) {
  return ["tagged", "error"].includes(state.trim().toLowerCase());
}

/**
 *
 */
function selectAllStudent() {
  $("#studentFiletable input:checkbox").prop("checked", true);
}

/**
 *
 */
/*function selectNoneStudent() {
  $("input:checkbox[class=chkstudent]").each(function () {
    $(this).prop("checked", false);
  });
}*/

/**
 *
 */
/*function selectInvertStudent() {
  $("input:checkbox[class=chkstudent]").each(function () {
    if ($(this).prop("checked") == true) {
      $(this).prop("checked", false);
    } else {
      $(this).prop("checked", true);
    }
  });
}*/

/**
 *
 */
function selectAllInstructor() {
  $("#instructorFiletable input:checkbox").prop("checked", true);
}

/**
 *
 */
/*function selectNoneInstructor() {
  $("input:checkbox[class=chkinstructor]").each(function () {
    $(this).prop("checked", false);
  });
}*/

/**
 *
 */
/*function selectInvertInstructor() {
  $("input:checkbox[class=chkinstructor]").each(function () {
    if ($(this).prop("checked") == true) {
      $(this).prop("checked", false);
    } else {
      $(this).prop("checked", true);
    }
  });
}*/

/**
{  
 "context_id":"15d07aef811f127ae42d2e7ce64549a2990fe268",
 "user_id":"12c0e80b9de773158315e0167beae5309e475d8a",
 "token":"751bd29c-94d1-4e6b-acea-89e1e9824a92",
 "sourceId":"",
 "coursename":"CTAT Testing",
 "studentnamefull":"Martin Van Velsen",
 "grade_passback":"https://cmu.test.instructure.com/api/lti/v1/tools/758/ext_grade_passback",
 "roles":"Instructor,urn:lti:instrole:ims/lis/Administrator",
 "resource_link_id":"f2f00bc0df6fd1a4dad2fed0a0b95387489605cc",
 "dictionary":"270 - Proposal"
}
*/
function settingsToQuerystring() {
  var queryString = "";

  queryString += "&context_id=" + settings["context_id"];
  queryString += "&user_id=" + settings["user_id"];
  queryString += "&token=" + settings["token"];
  queryString += "&resource_link_id=" + settings["resource_link_id"];
  queryString += "&roles=" + settings["roles"];

  return queryString;
}

function appendQuerySettings(url) {
  ["context_id", "user_id", "token", "resource_link_id", "roles"].forEach(
    (key) => url.searchParams.append(key, settings[key])
  );
}

/**
 *
 */
function refresh() {
  docdebug("refresh()");
  if (!("settings" in window)) return;

  // Dim the lights
  $(".fa-redo").addClass("dim");

  fetch(`${DOCUSCOPE_LTI}/list`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(settings),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`Server response not OK: ${response.status}`);
      }
      return response.json();
    })
    .then((fileData) => {
      docdebug("success");
      if (typeof data !== "undefined") return;

      // Add the ones that don't exist yet
      fileData.forEach((fileObject) => {
        const submitted = fileObject.state;
        const fCreatedDate = new Date(Number(fileObject.createdraw) * 1000);
        const fCreated = fCreatedDate.toLocaleString();

        // Let's see if the file already exists
        if (document.getElementById(fileObject.id) === null) {
          //var fileSize = getReadableFileSizeString(fileObject.size);
          const isStudent = ["student", "0"].includes(fileObject.ownedby);
          const row = $("<tr/>")
            .attr("id", `row-${fileObject.id}`)
            .addClass("rowindicator");
          // checkbox
          row.append(
            $("<td/>").append(
              $('<input type="checkbox">')
                .addClass(isStudent ? "chkstudent" : "chkinstructor")
                .addClass("ds-file-checkbox")
                .toggleClass("invisible", !isActive(submitted))
                .attr("id", fileObject.id)
                .attr("name", fileObject.name)
                .attr("value", fileObject.id)
                .prop("checked", !isError(submitted))
                .prop("disabled", !isActive(submitted))
            )
          );
          // filename
          row.append(
            $("<td/>")
              .attr("align", "left")
              .append(
                $("<a/>")
                  .text(fileObject.name)
                  .attr(
                    "href",
                    `${DOCUSCOPE_LTI}/downloadfile?id=${
                      fileObject.id
                    }${settingsToQuerystring()}`
                  )
              )
          );
          // Student name
          if (isStudent) {
            row.append(
              $("<td/>").attr("align", "left").text(fileObject.fullname)
            );
          }
          // State
          row.append(
            $("<td/>")
              .attr("data-fileid", fileObject.id)
              .addClass("filestate")
              .text(submitted)
          );
          // Created date
          row.append($("<td/>").addClass("text-end").text(fCreated));

          if (isStudent) {
            $("#studentFiletable tbody").append(row);
          } else {
            $("#instructorFiletable tbody").append(row);
          }
        } else {
          // Update the state since the entry already exists
          const update =
            $(
              `.filestate[data-fileid="${fileObject.id}"]:contains("${submitted}")`
            ).length === 0;
          if (update) {
            // only update on change.
            $(`.filestate[data-fileid="${fileObject.id}"]`).text(submitted);
            $(`#${fileObject.id}`)
              .toggleClass("invisible", !isActive(submitted))
              .prop("disabled", !isActive(submitted))
              .prop("checked", !isError(submitted));
          }
        }
      });

      // Remove the ones that don't exist any longer
      const fileIds = fileData.map(({ id }) => `#row-${id}`).join(",");
      $(`.rowindicator:not(${fileIds})`).remove();
    })
    .catch(console.error)
    .finally(() => {
      // Turn the lights back on
      $(".fa-redo").removeClass("dim");
    });
}

/**
 * Check if all checked rows are tagged.
 * @returns {bool}
 */
function readyCheck() {
  return $("input.ds-file-checkbox:checked:not(:disabled)")
    .map((_i, el) =>
      $(`.filestate[data-fileid="${el.id}"]`)
        .map((_i, filestate) => $(filestate).text())
        .get()
        .every((state) => isTagged(state))
    )
    .get()
    .every((tagged) => tagged);
}

/**
 * Return all of the checked files.
 * @returns {string[]}
 */
function getCheckedIds() {
  return $("input.ds-file-checkbox:checked:not(:disabled)")
    .map((_i, el) => el.id)
    .get();
}

/**
 *
 */
function submit() {
  docdebug("submit ()");

  if (!readyCheck()) {
    alert(
      "Warning: not all of your documents have been tagged and processed yet"
    );
    return;
  }

  const submittable = getCheckedIds();

  if (submittable.length === 0) {
    alert("Please select at least one document");
    return;
  }

  const boxplot = new URL(`${CLASSROOM.pathname}/boxplot`, CLASSROOM);
  boxplot.searchParams.append("roles", settings["roles"]);
  boxplot.searchParams.append("token", settings["token"]);
  boxplot.searchParams.append("ids", submittable.join());
  window.open(boxplot.toString());
}

/**
 * Launch the DocuScope classroom document comparison tool.
 */
function compare() {
  if (!readyCheck()) {
    alert("Warning: not all of the selected documents have been tagged yet.");
    return;
  }

  const files = getCheckedIds();
  if (files.length === 2) {
    const mtv = new URL(`${CLASSROOM.pathname}/mtv`, CLASSROOM);
    mtv.searchParams.append("roles", settings["roles"]);
    mtv.searchParams.append("token", settings["token"]);
    mtv.searchParams.append("ids", files.join());
    window.open(mtv.toString());
  } else {
    alert("Please select exactly two tagged documents for comparison.");
  }
}

/**
 *
 */
function trashSelected() {
  if (typeof settings === "undefined") return;

  const selectedTrash = $("#instructorFiletable input:checked").map(
    (_i, el) => el.id
  );

  fetch(`${DOCUSCOPE_LTI}/trash`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ ...settings, trash: selectedTrash }),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`Server response not OK: ${response.status}`);
      }
      return response.blob();
    })
    .then(() => docdebug("success"))
    .catch((error) => console.error(error))
    .finally(() => refresh());
}

/**
 * https://docs.jboss.org/author/display/WFLY8/Undertow+subsystem+configuration
 */
function uploadFile(aFile) {
  docdebug("uploadFile ()");

  docdebug("Checking upload size: " + aFile.size + " ...");
  if (aFile.size >= 5000000) {
    $("#uploadlabel").text(
      "Error: you can only upload files of 5 megabytes or less"
    );
    return;
  }

  var validExtension = false;

  if (aFile.name.toLowerCase().indexOf(".docx") != -1) {
    validExtension = true;
  }

  /*
  if (aFile.name.toLowerCase().indexOf (".pdf")!=-1) {
    validExtension=true;
  }
  */

  if (validExtension == false) {
    $("#uploadlabel").text(
      "Error: the file " + aFile.name + " is not a docx file"
    );
    return;
  }

  settings["ownedby"] = "instructor"; // Uploaded by instructor

  const d = new Date();
  const url = `${DOCUSCOPE_LTI}/upload`;
  const fd = new FormData();
  fd.append("file", aFile);
  fd.append("created", d.toISOString());
  fd.append("createdraw", d.getTime());
  fd.append("timezone", Intl.DateTimeFormat().resolvedOptions().timeZone);

  for (const [key, value] of Object.entries(settings)) {
    fd.append(key, value);
  }

  var xhr = new XMLHttpRequest();
  xhr.upload.addEventListener("progress", uploadProgress, false);
  xhr.addEventListener("load", uploadComplete, false);
  xhr.open("POST", url, true);
  xhr.send(fd);
}

/**
 *
 */
function uploadProgress(event) {
  // Note: doesn't work with async=false.
  var progress = Math.round((event.loaded / event.total) * 100);
  docdebug("Progress " + progress + "%");

  if (progress == 100) {
    refresh();
    remainingUpload--;
  }

  if (remainingUpload > 0) {
    $("#uploadlabel").text(
      "Uploading file " + remainingUpload + " of " + totalUpload
    );
  } else {
    $("#uploadlabel").text("Upload complete");

    setTimeout(function () {
      $("#uploadlabel").text("Choose a file");
    }, 5000);
  }
}

/**
 *
 */
function uploadComplete() {
  docdebug("uploadComplete ()");
  refresh();
}

/**
 *
 */
function onFileChange(files) {
  docdebug("onFileChange()");

  if (typeof settings !== "undefined") {
    // Use DataTransfer interface to access the file(s)
    docdebug("Uploading " + files.length + " files ...");

    remainingUpload = files.length;
    totalUpload = files.length;

    for (var i = 0; i < files.length; i++) {
      var targetFile = files[i];
      docdebug("... file[" + i + "].name = " + targetFile.name);
      uploadFile(targetFile);
    }
  }

  $("#file")[0].value = "";
}

function downloadDocuments(tableSelector) {
  const ids = $(`${tableSelector} input:checked:not(:disabled)`)
    .map((_i, el) => $(el).attr("value"))
    .get();

  if (ids.length === 0) {
    alert("Please select at least one document");
  } else {
    window.open(
      `{DOCUSCOPE_LTI}/downloadlist?ids=${ids.join()}${settingsToQuerystring()}`
    );
  }
}

function onDownloadStudent() {
  downloadDocuments("#studentFiletable");
}

function onDownloadInstructor() {
  downloadDocuments("#instructorFiletable");
}

/**
 *
 */
function onToggleModels() {
  docdebug("onToggleModels()");

  const toggleModels = $("#togglemodels").prop("checked");

  if (typeof settings !== "undefined") {
    const url = new URL(`${DOCUSCOPE_LTI}/togglemodels`, document.location);
    url.searchParams.append("toggle", toggleModels);
    appendQuerySettings(url);
    fetch(url.toString(), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(settings),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Server response not OK: ${response.status}`);
        }
        return response.blob();
      })
      .then(() => docdebug("success"))
      .catch((err) => console.error(err));
  }
}

/**
 *
 */
function onToggleStudent() {
  docdebug("onToggleStudent()");

  const toggleStudent = $("#togglestudent").prop("checked");

  if (typeof settings !== "undefined") {
    const url = new URL(`${DOCUSCOPE_LTI}/togglestudent`, document.location);
    url.searchParams.append("toggle", toggleStudent);
    appendQuerySettings(url);
    fetch(url.toString(), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(settings),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Server response not OK: ${response.status}`);
        }
        return response.blob();
      })
      .then(() => docdebug("success"))
      .catch((err) => console.error(err));
  }
}

/**
 *
 */
function dropHandler(event) {
  docdebug("dropHandler ()");

  event.preventDefault();
  event.stopPropagation();

  if (typeof settings !== "undefined") {
    if (event.dataTransfer.items) {
      docdebug("Uploading " + event.dataTransfer.items.length + " files ...");

      // Use DataTransferItemList interface to access the file(s)
      for (var i = 0; i < event.dataTransfer.items.length; i++) {
        remainingUpload = event.dataTransfer.items.length;
        totalUpload = event.dataTransfer.items.length;

        // If dropped items aren't files, reject them
        if (event.dataTransfer.items[i].kind === "file") {
          var targetFile = event.dataTransfer.items[i].getAsFile();
          docdebug("... file[" + i + "].name = " + targetFile.name);
          uploadFile(targetFile);
        }
      }
    } else {
      // Use DataTransfer interface to access the file(s)
      docdebug("Uploading " + event.dataTransfer.files.length + " files ...");

      remainingUpload = event.dataTransfer.files.length;
      totalUpload = event.dataTransfer.files.length;

      event.dataTransfer.files.forEach((targetFile, index) => {
        docdebug(`... file[${index}].name = ${targetFile.name}`);
        uploadFile(targetFile);
      });
    }
  }

  // Pass event to removeDragData for cleanup
  removeDragData(event);
}

/**
 * Prevent default behavior (Prevent file from being opened)
 */
function dragOverHandler(event) {
  //docdebug('dragOverHandler ()');
  event.preventDefault();
}

/**
 *
 */
function removeDragData(event) {
  docdebug("removeDragData ()");

  if (event.dataTransfer.items) {
    // Use DataTransferItemList interface to remove the drag data
    event.dataTransfer.items.clear();
  } else {
    // Use DataTransfer interface to remove the drag data
    event.dataTransfer.clearData();
  }
}

let idleTime = 0;
let tutorTimeout = 19;

/**
 *
 */
function setIdleTimeout() {
  docdebug("setIdleTimeout ()");

  // Increment the idle time counter every minute.
  setInterval(timerIncrement, 60000); // 1 minute

  // Zero the idle timer on mouse movement.
  $(document).on("mousemove", function () {
    //docdebug ("reset idle time");
    idleTime = 0;
  });

  $(document).on("keypress", function () {
    //docdebug ("reset idle time");
    idleTime = 0;
  });
}

/**
 *
 */
function timerIncrement() {
  //docdebug ("timerIncrement ()");

  if (idleTime < 0) {
    //docdebug ("We've already timed out, bump");
    return;
  }

  idleTime = idleTime + 1;

  if (idleTime > tutorTimeout) {
    // 20 minutes by default
    idleTime = -1;
    disableApp();
  }
}

/**
 *
 */
function setRefreshInterval() {
  docdebug("setRefreshInterval ()");
  updateTimerId = setInterval(refresh, 20000);
  refresh();
}

/**
 *
 */
function disableApp() {
  docdebug("disableApp ()");
  $("#scrim").show();
  clearInterval(updateTimerId);
}

/**
 *
 */
$(function () {
  $("#togglestudent").on("click", onToggleStudent);
  $("#togglemodels").on("click", onToggleModels);
  $(".dropcontainer, .dropcontainer label")
    .on("drop", dropHandler)
    .on("dragover", dragOverHandler)
    .on("change", onFileChange);
  $("#select").on("click", submit);
  $("#compare").on("click", compare);
  $(".refresh-button").on("click", refresh);
  $(".trash-button").on("click", trashSelected);
  $("#allStudent").on("click", selectAllStudent);
  $("#allInstructor").on("click", selectAllInstructor);
  $("#downloadStudent").on("click", onDownloadStudent);
  $("#downloadInstructor").on("click", onDownloadInstructor);

  if (typeof settings === "undefined") {
    docdebug("Error: no settings object available");
    return;
  }

  $("#togglestudent").prop("checked", settings["showstudent"]);
  $("#togglemodels").prop("checked", settings["showmodels"]);

  setIdleTimeout();
  setRefreshInterval();
});
