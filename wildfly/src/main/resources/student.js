/* global settings, $, bootstrap */

var remainingUpload = 0;
var totalUpload = 0;
var debugging = false;

const CLASSROOM = new URL("https://docuscope.eberly.cmu.edu/classroom/");

/**
 *
 */
function docdebug(aMessage) {
  if (debugging === true) {
    console.log(aMessage);
  }
}

/**
 * Test if the given state is "tagged".
 * @arg state {str}
 * @returns {bool}
 */
function isTagged(state) {
  return state.trim().toLowerCase() === "tagged";
}
/**
 * Test if the given state is "error".
 * @arg state {str}
 * @returns {bool}
 */
function isError(state) {
  return state.trim().toLowerCase() === "error";
}
/**
 * Test if the given state is "tagged" or "error".
 * @arg state {str}
 * @returns {bool}
 */
function isActive(state) {
  return ["tagged", "error"].includes(state.trim().toLowerCase());
}

/**
 *
 */
/*function selectAll() {
  $("input:checkbox[class=chk]").each(function () {
    $(this).prop("checked", true);
  });
}*/

/**
 *
 */
/*function selectNone() {
  $("input:checkbox[class=chk]").each(function () {
    $(this).prop("checked", false);
  });
}*/

/**
 *
 */
/*function selectInvert() {
  $("input:checkbox[class=chk]").each(function () {
    if ($(this).prop("checked") == true) {
      $(this).prop("checked", false);
    } else {
      $(this).prop("checked", true);
    }
  });
}*/

/**
 *
 */
function refresh() {
  if (typeof settings !== "undefined") {
    // Overwrite what the database says for this assignment type. This is a fix for v3
    // so that we don't have to remove all the code we already have for v4
    // settings ["showModels"]="false";

    fetch("/lti/activity/docuscope/list", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(settings),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Server response not OK: ${response.status}`);
        }
        return response.json();
      })
      .then((data) => {
        //docdebug ("success");
        if (typeof data !== "undefined") {
          showFiles(data);
        } else {
          $("#filetable > tbody > tr").remove();
          $("#filetable2 > tbody > tr").remove();
        }
      })
      .catch(console.error);
  }
}

/**
 *
 */
function settingsToQuerystring() {
  var queryString = "";

  queryString += "&context_id=" + settings["context_id"];
  queryString += "&user_id=" + settings["user_id"];
  queryString += "&token=" + settings["token"];
  queryString += "&resource_link_id=" + settings["resource_link_id"];
  queryString += "&roles=" + settings["roles"];

  return queryString;
}

/**
 * Launch the DocuScope classroom comparison tool.
 */
function compare() {
  const submittable = $("input.chk:checked")
    .map((_i, el) => el.id)
    .get();

  if (submittable.length === 0) {
    alert("Please select one or more documents");
    return;
  }

  const mtv = new URL(`${CLASSROOM.pathname}/mtv`, CLASSROOM);
  mtv.searchParams.append("roles", settings["roles"]);
  mtv.searchParams.append("token", settings["token"]);
  mtv.searchParams.append("ids", submittable.join());
  window.open(mtv.toString());
}

/**
 * Open a new window to DocuScope classroom's single text view.
 * @param {string} aFile uuid of file to view
 */
function submit(aFile) {
  const stv = new URL(`${CLASSROOM.pathname}/stv/${aFile}`, CLASSROOM);
  window.open(stv.toString());
}

/**
 *
 */
function showFiles(fileData) {
  // Assume that most recent is shown to instructor.
  // Sorting happens at the end.
  fileData.forEach((fileObject) => {
    const tagged = isTagged(fileObject.state);
    const fDate = new Date(Number(fileObject.createdraw) * 1000);

    const $existing = $(`tr[data-docuscope-file-id="${fileObject.id}"]`);
    if ($existing.length > 0) {
      $existing
        .toggleClass("tagged", tagged && fileObject.ownedby === "student")
        .attr("data-docuscope-created", fileObject.createdraw);
      $existing
        .find("button")
        .text(
          tagged ? "Visualize" : isError(fileObject.state) ? "Error" : "Pending"
        )
        .toggleClass("btn-danger", isError(fileObject.state))
        .toggleClass("btn-success", tagged)
        .toggleClass("btn-primary", !isActive(fileObject.state))
        .prop("disabled", !tagged || !settings["showstudent"]);
      $existing.find("model-status").text(!tagged ? "not tagged" : "tagged");
      $existing.find("modified-date").text(fDate.toLocaleString());
    } else {
      const row = $("<tr/>")
        .toggleClass("tagged", tagged && fileObject.ownedby === "student")
        .attr("data-docuscope-file-id", fileObject.id)
        .attr("data-docuscope-created", fileObject.createdraw);
      const col0 = $("<td/>").attr("align", "left");
      const filecheck = $("<div/>").addClass("form-check");
      filecheck.append(
        $('<input type="checkbox">')
          .addClass("chk form-check-input")
          .attr("id", fileObject.id)
          .attr("name", fileObject.id)
          .attr("value", fileObject.name)
      );
      filecheck.append(
        $("<label/>").append(
          $("<a/>")
            .attr(
              "href",
              `/lti/activity/docuscope/downloadfile?id=${
                fileObject.id
              }${settingsToQuerystring()}`
            )
            .text(fileObject.name)
        )
      );
      col0.append(filecheck);
      row.append(col0);
      let col1;
      if (fileObject.ownedby === "student") {
        col1 = $("<td/>").append(
          $("<button/>")
            .addClass("btn btn-sm visualize-button")
            .toggleClass("btn-danger", isError(fileObject.state))
            .toggleClass("btn-success", tagged)
            .toggleClass("btn-primary", !isActive(fileObject.state))
            .prop("disabled", !tagged || !settings["showstudent"])
            .attr("data-docuscope-file-id", fileObject.id)
            .on("click", function () {
              if (!$(this).prop("disabled")) {
                submit($(this).attr("data-docuscope-file-id"));
              }
            })
            .text(
              tagged
                ? "Visualize"
                : isError(fileObject.state)
                ? "Error"
                : "Pending"
            )
        );
      } else {
        col1 = $("<td/>")
          .addClass("model-status")
          .text(!tagged ? "not tagged" : "tagged");
      }
      row.append(col1);
      const col2 = $("<td/>")
        .addClass("text-end modified-date")
        .text(fDate.toLocaleString());
      row.append(col2);

      if (fileObject.ownedby === "student") {
        $("#filetable tbody").append(row);
      } else if (settings["showmodels"] == true) {
        $("#filetable2 tbody").append(row);
      }
    }
  });

  // Remove no longer available
  const fileIds = fileData
    .map(({ id }) => `[data-docuscope-file-id=${id}]`)
    .join(",");
  $(`tbody tr:not(${fileIds})`).remove();

  // Sort student items by reverse date.
  const student_list = $("#filetable tbody");
  const student_files = student_list.children("tr").get();
  student_files.sort(
    (a, b) =>
      parseInt($(b).attr("data-docuscope-created")) -
      parseInt($(a).attr("data-docuscope-created"))
  );
  $.each(student_files, function (idx, itm) {
    student_list.append(itm);
  });
  student_list.find("tr.table-primary").removeClass("border border-info");
  $("#hidden-storage").append($("#shown-to-instructor")); // reparent to prevent loss
  student_list.find("tr:first-child").addClass("border border-info");
  student_list
    .find("tr:first-child .form-check")
    .append($("#shown-to-instructor"));
}

/**
 *
 */
function uploadFile(aFile) {
  docdebug("uploadFile ()");

  docdebug("Checking upload size: " + aFile.size + " ...");

  /*
  if (aFile.size>=1000000) {
    $("#uploadlabel").text ("Error: you can only upload files of 1 megabytes or less");
    return;
  }
  */

  if (aFile.size >= 5000000) {
    $("#uploadlabel").text(
      "Error: you can only upload files of 5 megabytes or less"
    );
    return;
  }

  //var lower = aFile.name.toLowerCase();
  let validExtension = aFile.name.toLowerCase().endsWith(".docx");

  /*
  if (aFile.name.toLowerCase().indexOf (".pdf")!=-1) {
    validExtension=true;
  }
  */

  if (!validExtension) {
    //$("#uploadlabel").text ("Error: the file " + aFile.name + " is not a docx or pdf file");
    $("#uploadlabel").text(
      "Error: the file " + aFile.name + " is not a docx file"
    );
    return;
  }

  settings["ownedby"] = "student"; // Uploaded by student

  var d = new Date();

  console.log(Intl.DateTimeFormat().resolvedOptions().timeZone);

  var url = "/lti/activity/docuscope/upload";
  var fd = new FormData();
  fd.append("file", aFile);
  fd.append("created", d.toISOString()); // FIXME Unused
  fd.append("createdraw", d.getTime()); // FIXME Unused
  fd.append("timezone", Intl.DateTimeFormat().resolvedOptions().timeZone); // FIXME Unused

  for (const [key, value] of Object.entries(settings)) {
    docdebug(`Adding extra file info: ${key} -> ${value}`);
    fd.append(key, value);
  }

  var xhr = new XMLHttpRequest();
  xhr.upload.addEventListener("progress", uploadProgress, false);
  xhr.addEventListener("load", uploadComplete, false);
  xhr.open("POST", url, true);
  xhr.send(fd);
}

/**
 *
 */
function uploadProgress(event) {
  // Note: doesn't work with async=false.
  var progress = Math.round((event.loaded / event.total) * 100);
  docdebug("Progress " + progress + "%");

  if (progress == 100) {
    refresh();
    remainingUpload--;
  }

  if (remainingUpload > 0) {
    $("#uploadlabel").text(
      "Uploading file " + remainingUpload + " of " + totalUpload
    );
  } else {
    $("#uploadlabel").text("Upload complete");

    setTimeout(function () {
      $("#uploadlabel").text("Choose a file");
    }, 5000);
  }
}

/**
 *
 */
function uploadComplete() {
  docdebug("uploadComplete ()");
  refresh();
}

/**
 *
 */
function onFileChange(files) {
  docdebug("onFileChange()");

  if (typeof settings !== "undefined") {
    // Use DataTransfer interface to access the file(s)
    docdebug("Uploading " + files.length + " files ...");

    remainingUpload = files.length;
    totalUpload = files.length;

    for (var i = 0; i < files.length; i++) {
      var targetFile = files[i];
      docdebug("... file[" + i + "].name = " + targetFile.name);
      uploadFile(targetFile);
    }
  }

  $("#file")[0].value = "";
}

/**
 *
 */
function dropHandler(event) {
  docdebug("dropHandler()");

  event.preventDefault();
  event.stopPropagation();

  if (typeof settings !== "undefined") {
    if (event.dataTransfer.items) {
      docdebug("Uploading " + event.dataTransfer.items.length + " files ...");

      // Use DataTransferItemList interface to access the file(s)
      for (var i = 0; i < event.dataTransfer.items.length; i++) {
        remainingUpload = event.dataTransfer.items.length;
        totalUpload = event.dataTransfer.items.length;

        // If dropped items aren't files, reject them
        if (event.dataTransfer.items[i].kind === "file") {
          var targetFile = event.dataTransfer.items[i].getAsFile();
          docdebug("... file[" + i + "].name = " + targetFile.name);
          uploadFile(targetFile);
        }
      }
    } else {
      // Use DataTransfer interface to access the file(s)
      docdebug("Uploading " + event.dataTransfer.files.length + " files ...");

      remainingUpload = event.dataTransfer.items.length;
      totalUpload = event.dataTransfer.items.length;

      event.dataTransfer.files.forEach((targetFile, index) => {
        docdebug(`... file[${index}].name = ${targetFile.name}`);
        uploadFile(targetFile);
      });
    }
  }

  // Pass event to removeDragData for cleanup
  removeDragData(event);
}

/**
 * Prevent default behavior (Prevent file from being opened)
 */
function dragOverHandler(event) {
  //docdebug ("dragOverHandler ()");
  event.preventDefault();
}

/**
 *
 */
function removeDragData(event) {
  docdebug("removeDragData ()");

  if (event.dataTransfer.items) {
    // Use DataTransferItemList interface to remove the drag data
    event.dataTransfer.items.clear();
  } else {
    // Use DataTransfer interface to remove the drag data
    event.dataTransfer.clearData();
  }
}

var idleTime = 0;
var tutorTimeout = 19;

/**
 *
 */
function setIdleTimeout() {
  docdebug("setIdleTimeout ()");

  // Increment the idle time counter every minute.
  setInterval(timerIncrement, 60000); // 1 minute

  // Zero the idle timer on mouse movement.
  $(document).on("mousemove", function () {
    //docdebug ("reset idle time");
    idleTime = 0;
  });

  $(document).on("keypress", function () {
    //docdebug ("reset idle time");
    idleTime = 0;
  });
}

/**
 *
 */
function timerIncrement() {
  //docdebug ("timerIncrement ()");

  if (idleTime < 0) {
    return;
  }

  idleTime = idleTime + 1;

  if (idleTime > tutorTimeout) {
    // 20 minutes by default
    idleTime = -1;

    docdebug("Timeout reached, blocking activity ...");

    $("#scrim").show();
  }
}

/**
 *
 */
function setRefreshInterval() {
  docdebug("setRefreshInterval ()");
  setInterval(refresh, 20000);
}

/**
 *
 */
$(function () {
  $("#filecontainer, #filecontainer label")
    .on("drop", dropHandler)
    .on("dragover", dragOverHandler)
    .on("change", onFileChange);
  $("#compare").on("click", compare);
  $(".refresh-button").on("click", refresh);
  $("#download-model").on("click", () => {
    let ids = [];
    $("#filetable2 input:checked:not(:disabled)").each(function () {
      ids.push($(this).attr("value"));
    });
    ids = ids.filter((id) => id !== undefined);
    if (ids.length === 0) {
      alert("Please select at least one document");
    } else {
      window.open(
        `/lti/activity/docuscope/downloadlist?ids=${ids.join()}${settingsToQuerystring()}`
      );
    }
  });

  if (typeof settings === "undefined") {
    docdebug("Error: no settings object available");
    return;
  }

  $("#modeltexts").toggleClass("d-none", !settings["showmodels"]);
  $(".student-submitted").toggleClass("d-none", settings["showstudent"]);

  const tooltipTriggerList = document.querySelectorAll(
    '[data-bs-toggle="tooltip"]'
  );
  [...tooltipTriggerList].map(
    (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
  );

  setIdleTimeout();
  setRefreshInterval();
  refresh();
});
