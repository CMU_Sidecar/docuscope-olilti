<%--
Global error page
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>OLI LTI Error</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="resources/css/screen.css" />
</head>
<body>
    <div id="container">
        <div align="right" class="dualbrand">
            <img src="resources/gfx/rhjb_eap_logo.png" />
        </div>
        <div id="content">
        ${errorMessage}
        </div>
    </div>
</body>
</html>
