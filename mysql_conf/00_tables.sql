--
-- Database: `docuscope`
--

CREATE DATABASE IF NOT EXISTS docuscope;
USE docuscope;

--
-- Table structure for table `dictionaries`
--

/* DROP TABLE IF EXISTS `dictionaries` */;
CREATE TABLE IF NOT EXISTS `dictionaries` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` TINYTEXT NOT NULL, /*UNIQUE*/
  `class_info` JSON,
  `enabled` BOOLEAN NOT NULL DEFAULT True,
  PRIMARY KEY(`id`)
);
/*CREATE TABLE IF NOT EXISTS `dictionary_help` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `dictionary` SMALLINT UNSIGNED NOT NULL REFERENCES dictionaries(id),
  `ref` TINYTEXT NOT NULL,
  `name` TINYTEXT,
  `description` TEXT,
  `scope` ENUM('cluster', 'dimension') NOT NULL DEFAULT 'dimension',
  PRIMARY KEY(`id`)
);*/
-- class_info:
-- {"cluster|dimension": [{"id": <id>, "name": <name>, "description": <description>}]}

/* DROP TABLE IF EXISTS `assignments` */;
CREATE TABLE IF NOT EXISTS `assignments` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `oli_id` VARBINARY(20) NOT NULL UNIQUE, /* 20 byte uuid instead of standard 16?!? */
  `dictionary` SMALLINT UNSIGNED NOT NULL REFERENCES dictionaries(id),
  `name` TINYTEXT NOT NULL,
  `course` TINYTEXT NOT NULL,
  `instructor` TINYTEXT NOT NULL,
  `showstudent` BOOLEAN NOT NULL DEFAULT False,
  `showmodel` BOOLEAN NOT NULL DEFAULT False,
  `report_introduction` TEXT,
  `report_stv_introduction` TEXT
);

/* DROP TABLE IF EXISTS `filesystem` */;
CREATE TABLE IF NOT EXISTS `filesystem` (
  `id` VARBINARY(16) NOT NULL PRIMARY KEY,
  `name` TINYTEXT NOT NULL,
  `assignment` INTEGER NOT NULL REFERENCES assignments(id),
  `owner` TINYTEXT NOT NULL,
  `created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `timezone` TINYTEXT NOT NULL,
  `fullname` TINYTEXT NOT NULL,
  `state` ENUM('pending', 'submitted', 'tagged', 'error') NOT NULL,
  `ownedby` ENUM('student', 'instructor') NOT NULL,
  `content` LONGBLOB,
  `processed` JSON,
  `pdf` LONGBLOB
);
